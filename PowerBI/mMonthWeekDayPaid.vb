﻿Module mMonthWeekDayPaid
    Private SQL As String
    Private clC As New clsDBO
    Private sMonth As String
    Private sWeekDate As String

    Public Sub updatemonthweekdaypaid()
        SQL = "truncate table veritaspowerbi.dbo.contractcostbymonth "
        clC.RunSQL(SQL, sCON)
        SQL = "truncate table veritaspowerbi.dbo.contractcostbyweek "
        clC.RunSQL(SQL, sCON)
        SQL = "truncate table veritaspowerbi.dbo.contractcostbyday "
        clC.RunSQL(SQL, sCON)

        SQL = "select saledate as DatePaid, MoxyDealerCost from contract c
        where not DatePaid is null
        and not programid in (47,48,51,54,55,56,57,58,59,60,67,101,103,105)
        order by c.SaleDate "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            For cnt = 0 To clC.RowCount - 1
                clC.GetRowNo(cnt)
                If clC.Fields("datepaid") <> "1/1/1900" Then
                    ProcessMonth()
                    ProcessWeek()
                    ProcessDay()
                End If
            Next
        End If
    End Sub

    Private Sub ProcessDay()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.contractcostbyday "
        SQL = SQL + "where daydate = '" & Format(CDate(clC.Fields("datepaid")), "M/d/yyyy") & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("daydate") = Format(CDate(clC.Fields("datepaid")), "M/d/yyyy")
            clR.Fields("cnt") = 1
            clR.Fields("amt") = clC.Fields("moxydealercost")
            clR.AddRow()
            clR.SaveDB()
        Else
            clR.GetRow()
            clR.Fields("cnt") = CLng(clR.Fields("cnt")) + 1
            clR.Fields("amt") = CDbl(clR.Fields("amt")) + CDbl(clC.Fields("moxydealercost"))
            clR.SaveDB()
        End If

    End Sub

    Private Sub ProcessWeek()
        Dim clR As New clsDBO
        CalcWeek()
        SQL = "select * from veritaspowerbi.dbo.contractcostbyweek "
        SQL = SQL + "where weekdate = '" & sWeekDate & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("weekdate") = sWeekDate
            clR.Fields("cnt") = 1
            clR.Fields("amt") = clC.Fields("moxydealercost")
            clR.AddRow()
            clR.SaveDB()
        Else
            clR.GetRow()
            clR.Fields("cnt") = CLng(clR.Fields("cnt")) + 1
            clR.Fields("amt") = CDbl(clR.Fields("amt")) + CDbl(clC.Fields("moxydealercost"))
            clR.SaveDB()
        End If
    End Sub

    Private Sub CalcWeek()
        Dim lDay As Long
        lDay = Weekday(CDate(Format(CDate(clC.Fields("datepaid")), "M/d/yyyy")))
        sWeekDate = DateAdd(DateInterval.Day, (lDay - 1) * -1, CDate(Format(CDate(clC.Fields("datepaid")), "M/d/yyyy")))
    End Sub

    Private Sub ProcessMonth()
        Dim clR As New clsDBO
        CalcMonth()
        SQL = "select * from veritaspowerbi.dbo.contractcostbymonth "
        SQL = SQL + "where monthdate = '" & sMonth & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("monthdate") = sMonth
            clR.Fields("cnt") = 1
            clR.Fields("amt") = clC.Fields("moxydealercost")
            clR.AddRow()
            clR.SaveDB()
        Else
            clR.GetRow()
            clR.Fields("cnt") = CLng(clR.Fields("cnt")) + 1
            clR.Fields("amt") = CDbl(clR.Fields("amt")) + CDbl(clC.Fields("moxydealercost"))
            clR.SaveDB()
        End If
    End Sub

    Private Sub CalcMonth()
        sMonth = Month(CDate(clC.Fields("datepaid")))
        sMonth = sMonth + "/1/"
        sMonth = sMonth & Year(CDate(clC.Fields("datepaid")))
    End Sub
End Module
