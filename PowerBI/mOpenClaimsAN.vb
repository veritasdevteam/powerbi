﻿Module mOpenClaimsAN
    Private sStartMonth As String
    Private sEndMonth As String
    Private sCurMonth As String
    Private sLastMonth As String
    Private lCnt As Long

    Public Sub ClaimOpenAN()
        Dim lMonth As Long
        ClearTable()
        sStartMonth = "1/1/2020"
        sEndMonth = Month(Today) & "/1/" & Year(Today)
        lMonth = DateDiff(DateInterval.Month, CDate(sStartMonth), CDate(sEndMonth))
        For cnt = 0 To lMonth
            sCurMonth = DateAdd(DateInterval.Month, cnt, CDate(sStartMonth))
            sLastMonth = DateAdd(DateInterval.Month, 1, CDate(sCurMonth))
            lCnt = 0
            GetCount()
            InsertRecord()
        Next
        CalcRunRate()
    End Sub

    Private Sub CalcRunRate()
        Dim clR As New clsDBO
        Dim lDays As Long
        Dim lTotalDays As Long
        lDays = DateDiff(DateInterval.Day, CDate(sCurMonth), Today)
        lTotalDays = DateDiff(DateInterval.Day, CDate(sCurMonth), CDate(sLastMonth))
        SQL = "select * from veritaspowerbi.dbo.openclaimsAN "
        SQL = SQL + "where month = '" & Month(Today) & "/1/" & Year(Today) & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If lDays > 0 Then
                clR.Fields("cnt") = CInt((CLng(clR.Fields("cnt")) / lDays) * lTotalDays)
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub ClearTable()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.openclaimsan"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub InsertRecord()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.openclaimsan "
        SQL = SQL + "(month, cnt) "
        SQL = SQL + "values ('" & sCurMonth & "', " & lCnt & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub GetCount()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from claim "
        SQL = SQL + "where credate >= '" & sCurMonth & "' "
        SQL = SQL + "and credate < '" & sLastMonth & "' "
        SQL = SQL + "and status <> 'void' "
        SQL = SQL + "and contractid in (select contractid from contract "
        SQL = SQL + "where an <> 0) "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
        End If
    End Sub

End Module
