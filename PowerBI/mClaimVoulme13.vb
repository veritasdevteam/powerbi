﻿Module mClaimVoulme13
    Private sDate As String
    Private sStartDate As String
    Private sEndDate As String
    Private lWeek As Long

    Public Sub CalcVolume13()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.Week13CallVolumeV2 "
        clR.RunSQL(SQL, sCON)
        'sStartDate = "1/1/2022"
        'sStartDate = DateAdd(DateInterval.Day, -91, Today)
        'sEndDate = Today
        lWeek = 91 'DateDiff(DateInterval.Day, CDate(sStartDate), CDate(sEndDate))
        lWeek = lWeek / 7
        'lWeek = lWeek + 1
        For cnt = 1 To lWeek
            sEndDate = DateAdd(DateInterval.Day, cnt * 7, DateAdd(DateInterval.Day, -91, Today)) 'CDate("10/2/2021"))
            sStartDate = DateAdd(DateInterval.Day, -7, CDate(sEndDate))
            InsertRows()
            Get13WeekRollingCallVolumes()
        Next
        SQL = "update veritaspowerbi.dbo.Week13CallVolumeV2 "
        SQL = SQL + "set outbound = cast(outbound as float), "
        SQL = SQL + "inbound = cast(inbound as float) "
        clR.RunSQL(SQL, sCON)

    End Sub

    Private Sub Get13WeekRollingCallVolumes()
        Dim clC As New clsDBO
        Dim clR As New clsDBO
        Dim DeptName = ""
        SQL = "select 'Claim' as TeamName, sum(inboundct) as INC, sum(outboundct) obc from VeritasPhone.dbo.AgentDailyCallSummary ac " +
              "Left join veritas.dbo.UserSecurityInfo usi on usi.UserID = ac.UserID " +
              "left join veritas.dbo.UserTeam ut on ut.TeamID = usi.TeamID " +
              "where calldate >= '" & sStartDate & "' " +
              "and calldate < '" & sEndDate & "' " +
              "and ut.TeamID in (1,2,3,4,5,11) " +
              "Union " +
              "select TeamName, sum(inboundct) as INC, sum(outboundct) obc from VeritasPhone.dbo.AgentDailyCallSummary ac " +
              "left join veritas.dbo.UserSecurityInfo usi on usi.UserID = ac.UserID " +
              "left join veritas.dbo.UserTeam ut on ut.TeamID = usi.TeamID " +
              "where calldate >= '" & sStartDate & "' " +
              "and calldate < '" & sEndDate & "' " +
              "and ut.TeamID in (7,8,9) " +
              "group by TeamName " +
              "order by TeamName "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            For cnt = 0 To clC.RowCount - 1
                If cnt = 0 Then
                    DeptName = "Payments"
                ElseIf cnt = 1 Then
                    DeptName = "Claims"
                ElseIf cnt = 2 Then
                    DeptName = "CS"
                ElseIf cnt = 3 Then
                    DeptName = "Dealer Services"
                End If
                clC.GetRowNo(cnt)
                SQL = "update veritaspowerbi.dbo.Week13CallVolumeV2 "
                SQL = SQL + "set outbound = " & clC.Fields("obc") & ", "
                SQL = SQL + "inbound = " & clC.Fields("inc") & " "
                SQL = SQL + "where statdate = '" & sEndDate & "' "
                SQL = SQL + "and calldept = '" & DeptName & "' "
                clR.RunSQL(SQL, sCON)
            Next
        End If
    End Sub

    Private Sub InsertRows()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.Week13CallVolumeV2 "
        SQL = SQL + "(calldept, statdate, outbound, inbound) "
        SQL = SQL + "values ('Claims', '" & sEndDate & "', 0,0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.Week13CallVolumeV2 "
        SQL = SQL + "(calldept, statdate, outbound, inbound) "
        SQL = SQL + "values ('CS', '" & sEndDate & "', 0,0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.Week13CallVolumeV2 "
        SQL = SQL + "(calldept, statdate, outbound, inbound) "
        SQL = SQL + "values ('Dealer Services', '" & sEndDate & "', 0,0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.Week13CallVolumeV2 "
        SQL = SQL + "(calldept, statdate, outbound, inbound) "
        SQL = SQL + "values ('Payments', '" & sEndDate & "', 0,0)"
        clR.RunSQL(SQL, sCON)
    End Sub

End Module
