﻿Module mCallsOpenClaims13Week
    Private clWeek As New clsDBO
    Private clW As New clsDBO
    Private sStartWeek As String
    Private sEndWeek As String
    Private lCalls As Long
    Private lClaims As Long
    Private sPeriodDate As String
    Private sType As String
    Private dPer As Double

    Public Sub ProcessCallsOpenClaims13Week()
        SQL = "truncate table veritaspowerbi.dbo.callsopenclaims13week "
        clW.RunSQL(SQL, sCON)
        SQL = "select * from veritaspowerbi.dbo.weekdates "
        SQL = SQL + "where startdate <= '" & Today & "' "
        SQL = SQL + "and weekdate >= '4/5/2021' "
        SQL = SQL + "order by weekdate"
        clWeek.OpenDB(SQL, sCON)
        If clWeek.RowCount > 0 Then
            For cnt = 0 To clWeek.RowCount - 1
                clWeek.GetRowNo(cnt)
                CalcWeeksAll()
                CalcWeeksNonAN()
                CalcWeeksAN()
            Next
        End If
    End Sub

    Private Sub CalcWeeksAll()
        lCalls = 0
        lClaims = 0
        dPer = 0
        sPeriodDate = clWeek.Fields("weekdate")
        sType = "All"
        sEndWeek = clWeek.Fields("enddate")
        sStartWeek = clWeek.Fields("startdate")
        SQL = "select * from veritaspowerbi.dbo.weekdates "
        SQL = SQL + "where weekdate >= '" & sStartWeek & "' "
        SQL = SQL + "and weekdate <= '" & sEndWeek & "' "
        clW.OpenDB(SQL, sCON)
        If clW.RowCount > 0 Then
            For cnt = 0 To clW.RowCount - 1
                clW.GetRowNo(cnt)
                CalcAllClaims()
            Next
            dPer = lCalls / lClaims
            InsertAll()
        End If
    End Sub

    Private Sub CalcAllClaims()
        CalcCallsAll()
        CalcAllClaims2()
    End Sub

    Private Sub InsertAll()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.callsopenclaims13week "
        SQL = SQL + "(perioddate, type, totalcalls, totalclaims, totalper) "
        SQL = SQL + "values ("
        SQL = SQL + "'" & sPeriodDate & "', "
        SQL = SQL + "'" & sType & "', "
        SQL = SQL & lCalls & ", "
        SQL = SQL & lClaims & ", "
        SQL = SQL & dPer & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcAllClaims2()
        Dim clR As New clsDBO
        SQL = "select sum(dayall) as cnt from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where opendate >= '" & clW.Fields("startdate") & "' "
        SQL = SQL + "and opendate <= '" & clW.Fields("enddate") & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lClaims = lClaims + clR.Fields("cnt")
            End If
        End If
        SQL = "select sum(dayall) as cnt from veritaspowerbi.dbo.claimsopenan "
        SQL = SQL + "where opendate >= '" & clW.Fields("startdate") & "' "
        SQL = SQL + "and opendate <= '" & clW.Fields("enddate") & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lClaims = lClaims + clR.Fields("cnt")
            End If
        End If

    End Sub

    Private Sub CalcCallsAll()
        Dim clR As New clsDBO
        SQL = "select sum(callcount) as cnt from veritasphone.dbo.groupdailycallvolume "
        SQL = SQL + "where calldate >= '" & clW.Fields("startdate") & "' "
        SQL = SQL + "and calldate <= '" & clW.Fields("enddate") & "' "
        SQL = SQL + "and callgroup in ('AN Non-PT', 'AN Powertrain', 'Gen Claims') "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lCalls = lCalls + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcWeeksNonAN()
        lCalls = 0
        lClaims = 0
        dPer = 0
        sPeriodDate = clWeek.Fields("weekdate")
        sType = "Non-AN"
        sEndWeek = clWeek.Fields("enddate")
        sStartWeek = clWeek.Fields("startdate")
        SQL = "select * from veritaspowerbi.dbo.weekdates "
        SQL = SQL + "where weekdate >= '" & sStartWeek & "' "
        SQL = SQL + "and weekdate <= '" & sEndWeek & "' "
        clW.OpenDB(SQL, sCON)
        If clW.RowCount > 0 Then
            For cnt = 0 To clW.RowCount - 1
                clW.GetRowNo(cnt)
                CalcNonANClaims()
            Next
            dPer = lCalls / lClaims
            InsertAll()
        End If
    End Sub

    Private Sub CalcNonANClaims()
        CalcCallsNonAN()
        CalcNonANClaims2()
    End Sub

    Private Sub CalcNonANClaims2()
        Dim clR As New clsDBO
        SQL = "select sum(dayall) as cnt from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where opendate >= '" & clW.Fields("startdate") & "' "
        SQL = SQL + "and Opendate <= '" & clW.Fields("enddate") & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lClaims = lClaims + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcCallsNonAN()
        Dim clR As New clsDBO
        SQL = "select sum(callcount) as cnt from veritasphone.dbo.groupdailycallvolume "
        SQL = SQL + "where calldate >= '" & clW.Fields("startdate") & "' "
        SQL = SQL + "and calldate <= '" & clW.Fields("enddate") & "' "
        SQL = SQL + "and callgroup in ('Gen Claims') "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lCalls = lCalls + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcWeeksAN()
        lCalls = 0
        lClaims = 0
        dPer = 0
        sPeriodDate = clWeek.Fields("weekdate")
        sType = "AN"
        sEndWeek = clWeek.Fields("enddate")
        sStartWeek = clWeek.Fields("startdate")
        SQL = "select * from veritaspowerbi.dbo.weekdates "
        SQL = SQL + "where weekdate >= '" & sStartWeek & "' "
        SQL = SQL + "and weekdate <= '" & sEndWeek & "' "
        clW.OpenDB(SQL, sCON)
        If clW.RowCount > 0 Then
            For cnt = 0 To clW.RowCount - 1
                clW.GetRowNo(cnt)
                CalcANClaims()
            Next
            dPer = lCalls / lClaims
            InsertAll()
        End If
    End Sub

    Private Sub CalcANClaims()
        CalcCallsAN()
        CalcANClaims2()
    End Sub

    Private Sub CalcANClaims2()
        Dim clR As New clsDBO
        SQL = "select sum(dayall) as cnt from veritaspowerbi.dbo.claimsopenan "
        SQL = SQL + "where opendate >= '" & clW.Fields("startdate") & "' "
        SQL = SQL + "and opendate <= '" & clW.Fields("enddate") & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lClaims = lClaims + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcCallsAN()
        Dim clR As New clsDBO
        SQL = "select sum(callcount) as cnt from veritasphone.dbo.groupdailycallvolume "
        SQL = SQL + "where calldate >= '" & clW.Fields("startdate") & "' "
        SQL = SQL + "and calldate <= '" & clW.Fields("enddate") & "' "
        SQL = SQL + "and callgroup in ('AN Non-PT', 'AN Powertrain') "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lCalls = lCalls + clR.Fields("cnt")
            End If
        End If
    End Sub


End Module
