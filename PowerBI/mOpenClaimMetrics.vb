﻿Module mOpenClaimMetrics
    Private CurDate As Date

    Public Sub OpenClaimMetrics()
        Dim Date1 As Date = Today
        Dim Date2 As Date = New Date(2021, 8, 2)
        Dim iDaysDiff As Integer = CType(Date1 - Date2, TimeSpan).Days
        Dim cnt As Double = 0
        ClearTable()

        Do
            CurDate = Date2.AddDays(cnt)
            CalcTotalClaims()
            CalcOnlineClaims()
            CalcPhoneClaims()
            CalcOnlineClaimsAN()
            CalcPhoneClaimsAN()
            cnt += 1
        Loop While (cnt < iDaysDiff)
    End Sub

    Private Sub CalcPhoneClaimsAN()
        Dim clR As New clsDBO
        SQL = "insert into Veritaspowerbi.dbo.openclaimmetrics " +
            "(ClaimOpenDate, ClaimType, Cnt) " +
            "select '" + CurDate + "', 'Phone Claims AN', count(*) from claim c " +
            "where c.CreDate >= '" + CurDate + "' " +
            "and c.CreDate < '" + CurDate.AddDays(1) + "' " +
            "and c.contractid in (select contractid from contract where contractno like '%an%') " +
            "and not c.claimno in (select ClaimNo from VeritasClaimsAN.dbo.Claim where not claimno is null) "
        clR.RunSQL(SQL, sCON)
    End Sub
    Private Sub CalcOnlineClaimsAN()
        Dim clR As New clsDBO
        SQL = "insert into Veritaspowerbi.dbo.openclaimmetrics " +
            "(ClaimOpenDate, ClaimType, Cnt) " +
            "select '" + CurDate + "', 'Online Claims AN', count(*) from claim c " +
            "inner join VeritasClaimsAN.dbo.claim cl on cl.ClaimNo = c.ClaimNo " +
            "where c.CreDate >= '" + CurDate + "' " +
            "and c.CreDate < '" + CurDate.AddDays(1) + "' " +
            "and c.contractid in (select contractid from contract where contractno like '%an%') "
        clR.RunSQL(SQL, sCON)
    End Sub
    Private Sub CalcPhoneClaims()
        Dim clR As New clsDBO
        SQL = "insert into Veritaspowerbi.dbo.openclaimmetrics " +
            "(ClaimOpenDate, ClaimType, Cnt) " +
            "select '" + CurDate + "', 'Phone Claims', count(*) from claim c " +
            "where c.CreDate >= '" + CurDate + "' " +
            "and c.CreDate < '" + CurDate.AddDays(1) + "' " +
            "and c.contractid in (select contractid from contract where not contractno like '%an%') " +
            "and not c.claimno in (select ClaimNo from VeritasClaims.dbo.Claim where not claimno is null) "
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcOnlineClaims()
        Dim clR As New clsDBO
        SQL = "insert into Veritaspowerbi.dbo.openclaimmetrics " +
            "(ClaimOpenDate, ClaimType, Cnt) " +
            "select '" + CurDate + "', 'Online Claims', count(*) from claim c " +
            "inner join VeritasClaims.dbo.claim cl on cl.ClaimNo = c.ClaimNo " +
            "where c.CreDate >= '" + CurDate + "' " +
            "and c.CreDate < '" + CurDate.AddDays(1) + "' " +
            "and c.contractid in (select contractid from contract where not contractno like '%an%') "
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcTotalClaims()
        Dim clR As New clsDBO
        SQL = "insert into Veritaspowerbi.dbo.openclaimmetrics " +
            "(ClaimOpenDate, ClaimType, Cnt) " +
            "select '" + CurDate + "', 'Total Claims', count(*) from claim " +
            "where CreDate >= '" + CurDate + "' " +
            "and CreDate < '" + CurDate.AddDays(1) + "' "
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub ClearTable()
        Dim clR As New clsDBO
        SQL = "truncate table Veritaspowerbi.dbo.openclaimmetrics "
        clR.RunSQL(SQL, sCON)
    End Sub
End Module
