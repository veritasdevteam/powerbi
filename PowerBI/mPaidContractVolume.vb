﻿Module mPaidContractVolume
    Private sWeekDate As String
    Private sEndWeekDate As String
    Private lCnt As Long
    Private dAmt As Double

    Public Sub PaidContractVolume()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "truncate table veritaspowerbi.dbo.PaidContractVolume "
        clR.RunSQL(SQL, sCON)

        sWeekDate = DateAdd(DateInterval.Day, -91, Today)

        For cnt = 1 To 13
            sEndWeekDate = DateAdd(DateInterval.Day, 7, CDate(sWeekDate))
            CalcCount()
            CalcAmt()
            SQL = "insert into veritaspowerbi.dbo.paidcontractvolume "
            SQL = SQL + "(weekdate, cnt, amt) "
            SQL = SQL + "values ("
            SQL = SQL + "'" & sWeekDate & "', "
            SQL = SQL & lCnt & ", " & dAmt & ")"
            clR.RunSQL(SQL, sCON)
            sWeekDate = DateAdd(DateInterval.Day, 7, CDate(sWeekDate))
        Next
    End Sub

    Private Sub CalcAmt()
        Dim SQL As String
        Dim clR As New clsDBO
        dAmt = 0
        SQL = "select sum(moxydealercost) as amt from contract "
        SQL = SQL + "where datepaid >= '" & sWeekDate & "' "
        SQL = SQL + "and datepaid < '" & sEndWeekDate & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            dAmt = clR.Fields("amt")
        End If
    End Sub

    Private Sub CalcCount()
        Dim SQL As String
        Dim clR As New clsDBO
        lCnt = 0
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where datepaid >= '" & sWeekDate & "' "
        SQL = SQL + "and datepaid < '" & sEndWeekDate & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
        End If
    End Sub

End Module
