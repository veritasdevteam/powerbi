﻿Module mSalesVolumeAgentWeek
    Private sStartWeek As String
    Private sEndWeek As String
    Private sStartPrevWeek As String
    Private sEndPrevWeek As String
    Private lCurWeek As Long
    Private lPrevWeek As Long
    Private clM As New clsDBO
    Private clA As New clsDBO

    Public Sub SalesVolumeAgentWeek()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.SalesVolumeAgentWeek "
        clR.RunSQL(SQL, sCON)
        sEndWeek = Today
        sStartWeek = DateAdd(DateInterval.Day, -7, CDate(sEndWeek))
        sEndPrevWeek = sStartWeek
        sStartPrevWeek = DateAdd(DateInterval.Day, -7, CDate(sEndPrevWeek))

        'CalcMGA()
        CalcAgent()

    End Sub

    Private Sub CalcAgent()
        SQL = "select agentname from veritasmoxy.dbo.Moxycontract "
        SQL = SQL + "where solddate >= '" & sStartPrevWeek & "' "
        SQL = SQL + "and not contractno is null "
        SQL = SQL + "group by agentname "
        clM.OpenDB(SQL, sCON)
        If clM.RowCount > 0 Then
            For cnt = 0 To clM.RowCount - 1
                clM.GetRowNo(cnt)
                lCurWeek = 0
                GetCurWeekagent()
                GetPrevWeekAgent()
                AddRecordAgent()
            Next
        End If
    End Sub

    Private Sub CalcMGA()
        SQL = "select mga from veritasmoxy.dbo.Moxycontract "
        SQL = SQL + "where solddate >= '" & sStartPrevWeek & "' "
        SQL = SQL + "and not contractno is null "
        SQL = SQL + "and mga in ('C. Berman Associates, Inc.', 'Peak Performance Team Inc',
        'Dealers Choice INC','MIAMI Dade Mediation','Fleet Warranty Freddy Coleman',
        'The F&I Group Rick Hern', 'Vero LLC','Platinum Level', 'ADS, INC John OBrien') "
        SQL = SQL + "group by mga "
        clM.OpenDB(SQL, sCON)
        If clM.RowCount > 0 Then
            For cnt = 0 To clM.RowCount - 1
                clM.GetRowNo(cnt)
                lCurWeek = 0
                GetCurWeek()
                GetPrevWeek()
                addrecord
            Next
        End If
    End Sub

    Private Sub AddRecordAgent()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.salesvolumeagentweek "
        SQL = SQL + "(agentname, curweek, prevweek, diffweek) "
        SQL = SQL + "values ('"
        SQL = SQL + clM.Fields("AgentName") & "', "
        SQL = SQL & lCurWeek & ", "
        SQL = SQL & lPrevWeek & ", "
        SQL = SQL & lCurWeek - lPrevWeek & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub AddRecord()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.salesvolumeagentweek "
        SQL = SQL + "(agentname, curweek, prevweek, diffweek) "
        SQL = SQL + "values ('"
        SQL = SQL + clM.Fields("mga") & "', "
        SQL = SQL & lCurWeek & ", "
        SQL = SQL & lPrevWeek & ", "
        SQL = SQL & lCurWeek - lPrevWeek & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub GetPrevWeekAgent()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritasmoxy.dbo.Moxycontract "
        SQL = SQL + "where agentname = '" & clM.Fields("agentname") & "' "
        SQL = SQL + "and solddate >= '" & sStartPrevWeek & "' "
        SQL = SQL + "and solddate < '" & sEndPrevWeek & "' "
        SQL = SQL + "and not contractno is null "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lPrevWeek = clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub GetPrevWeek()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritasmoxy.dbo.Moxycontract "
        SQL = SQL + "where mga = '" & clM.Fields("mga") & "' "
        SQL = SQL + "and solddate >= '" & sStartPrevWeek & "' "
        SQL = SQL + "and solddate < '" & sEndPrevWeek & "' "
        SQL = SQL + "and not contractno is null "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lPrevWeek = clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub GetCurWeekAgent()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritasmoxy.dbo.Moxycontract "
        SQL = SQL + "where agentname = '" & clM.Fields("agentname") & "' "
        SQL = SQL + "and solddate >= '" & sStartWeek & "' "
        SQL = SQL + "and solddate < '" & sEndWeek & "' "
        SQL = SQL + "and not contractno is null "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lCurWeek = clR.Fields("cnt")
            End If
        End If

    End Sub

    Private Sub GetCurWeek()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritasmoxy.dbo.Moxycontract "
        SQL = SQL + "where mga = '" & clM.Fields("mga") & "' "
        SQL = SQL + "and solddate >= '" & sStartWeek & "' "
        SQL = SQL + "and solddate < '" & sEndWeek & "' "
        SQL = SQL + "and not contractno is null "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lCurWeek = clR.Fields("cnt")
            End If
        End If

    End Sub
End Module
