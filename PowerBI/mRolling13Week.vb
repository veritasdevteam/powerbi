﻿Module mRolling13Week
    Private clWeek As New clsDBO
    Private sStartWeek As String
    Private sEndWeek As String
    Private clW As New clsDBO
    Private lCnt As Long
    Private lCntAN As Long

    Public Sub Process13Week()
        SQL = "truncate table veritaspowerbi.dbo.claimrolling13week"
        clW.RunSQL(SQL, sCON)
        SQL = "select * from veritaspowerbi.dbo.weekdates "
        SQL = SQL + "where startdate <= '" & Today & "' "
        SQL = SQL + "and weekdate >= '4/5/2021' "
        SQL = SQL + "order by weekdate desc "
        clWeek.OpenDB(SQL, sCON)
        If clWeek.RowCount > 0 Then
            For cnt = 0 To clWeek.RowCount - 1
                clWeek.GetRowNo(cnt)
                If cnt = 0 Then
                    CalcRunRate()
                Else
                    CalcWeeks()
                End If
            Next
        End If
    End Sub

    Private Sub CalcRunRate()
        sEndWeek = clWeek.Fields("enddate")
        sStartWeek = clWeek.Fields("startdate")
        Dim ldays As Long
        Dim dCnt As Double
        lCnt = 0
        lCntAN = 0
        SQL = "select * from veritaspowerbi.dbo.weekdates "
        SQL = SQL + "where weekdate = '" & sStartWeek & "' "
        clW.OpenDB(SQL, sCON)
        If clW.RowCount > 0 Then
            For cnt = 0 To clW.RowCount - 1
                clW.GetRowNo(cnt)
                GetCount()
                GetCountAN()
            Next
        End If
        ldays = DateDiff(DateInterval.Day, CDate(sStartWeek), Today)
        ldays = ldays + 1
        dCnt = lCnt / ldays
        lCnt = dCnt * 5
        dCnt = lCntAN / ldays
        lCntAN = dCnt * 5
        InsertInfo()
    End Sub

    Private Sub CalcWeeks()
        sEndWeek = clWeek.Fields("enddate")
        sStartWeek = clWeek.Fields("startdate")
        lCnt = 0
        lCntAN = 0
        SQL = "select * from veritaspowerbi.dbo.weekdates "
        SQL = SQL + "where weekdate = '" & sStartWeek & "' "
        clW.OpenDB(SQL, sCON)
        If clW.RowCount > 0 Then
            For cnt = 0 To clW.RowCount - 1
                clW.GetRowNo(cnt)
                GetCount()
                GetCountAN()
            Next
        End If
        lCnt = lCnt
        lCntAN = lCntAN
        InsertInfo()

    End Sub

    Private Sub InsertInfo()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.Claimrolling13week "
        SQL = SQL + "(type, weekdate, cnt) "
        SQL = SQL + "values ('All', '" & clWeek.Fields("weekdate") & "', " & lCnt & ")"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.Claimrolling13week "
        SQL = SQL + "(type, weekdate, cnt) "
        SQL = SQL + "values ('AN', '" & clWeek.Fields("weekdate") & "', " & lCntAN & ")"
        clR.RunSQL(SQL, sCON)

    End Sub

    Private Sub GetCount()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from claim "
        SQL = SQL + "where credate >= '" & clW.Fields("startdate") & "' "
        SQL = SQL + "and credate <= '" & clW.Fields("enddate") & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = lCnt + clR.Fields("cnt")
        End If
    End Sub
    Private Sub GetCountAN()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from claim "
        SQL = SQL + "where credate >= '" & clW.Fields("startdate") & "' "
        SQL = SQL + "and credate <= '" & clW.Fields("enddate") & "' "
        SQL = SQL + "and contractid in (select contractid from contract where an <>0) "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCntAN = lCntAN + clR.Fields("cnt")
        End If
    End Sub

End Module
