﻿Module mClaimPercentage
    Private sStartDate As String
    Private sEndDate As String
    Private sStatDate As String

    Public Sub ClaimPercentage()
        Dim clR As New clsDBO
        ClearTable()
        sStartDate = Month(Today) & "/1/" & (Year(Today) - 2)
        For cnt = 0 To 24
            SQL = "insert into veritaspowerbi.dbo.claimpercentage "
            SQL = SQL + "(type, statmonth, contractcnt, claimcount, claimpercent) "
            SQL = SQL + "values( 'All', '" & DateAdd(DateInterval.Month, cnt, CDate(sStartDate)) & "',0,0,0)"
            clR.RunSQL(SQL, sCON)
            SQL = "insert into veritaspowerbi.dbo.claimpercentage "
            SQL = SQL + "(type, statmonth, contractcnt, claimcount, claimpercent) "
            SQL = SQL + "values( 'AN', '" & DateAdd(DateInterval.Month, cnt, CDate(sStartDate)) & "',0,0,0)"
            clR.RunSQL(SQL, sCON)
            sEndDate = DateAdd(DateInterval.Month, 1, DateAdd(DateInterval.Month, cnt, CDate(sStartDate)))
            sStatDate = DateAdd(DateInterval.Month, cnt, CDate(sStartDate))
            CalcContractPendingPaid()
            CalcContractPendingPaidAN()
            CalcContractCancel()
            CalcContractCancelAN()
            CalcContractExpire()
            CalcContractExpireAN()
            CalcClaim()
            CalcClaimAN()
        Next
        SQL = "update veritaspowerbi.dbo.claimpercentage "
        SQL = SQL + "set claimpercent = cast(contractcnt as float) / cast(claimcount as float) "
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcClaimAN()
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select count(*) as cnt from claim "
        SQL = SQL + "where credate < '" & sEndDate & "' "
        SQL = SQL + "and contractid in (select contractid from contract where an <> 0) "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.claimpercentage "
            SQL = SQL + "set claimcount = " & clC.Fields("cnt") & " "
            SQL = SQL + "where type = 'AN' "
            SQL = SQL + "and statmonth = '" & sStatDate & "' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub CalcClaim()
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select count(*) as cnt from claim "
        SQL = SQL + "where credate < '" & sEndDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.claimpercentage "
            SQL = SQL + "set claimcount = " & clC.Fields("cnt") & " "
            SQL = SQL + "where type = 'All' "
            SQL = SQL + "and statmonth = '" & sStatDate & "' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub ClearTable()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.claimpercentage "
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcContractExpireAN()
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'Expired' "
        SQL = SQL + "and saledate < '" & sEndDate & "' "
        SQL = SQL + "and expdate >= '" & sStatDate & "' "
        SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,103,105) "
        SQL = SQL + "and an <> 0 "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.claimpercentage "
            SQL = SQL + "set contractcnt = contractcnt + " & clC.Fields("cnt") & " "
            SQL = SQL + "where type = 'AN' "
            SQL = SQL + "and statmonth = '" & sStatDate & "' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub CalcContractExpire()
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'Expired' "
        SQL = SQL + "and saledate < '" & sEndDate & "' "
        SQL = SQL + "and expdate >= '" & sStatDate & "' "
        SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,103,105)"
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.claimpercentage "
            SQL = SQL + "set contractcnt = contractcnt + " & clC.Fields("cnt") & " "
            SQL = SQL + "where type = 'All' "
            SQL = SQL + "and statmonth = '" & sStatDate & "' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub CalcContractCancelAN()
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select count(distinct c.contractid) as cnt from contract c "
        SQL = SQL + "inner join contractcancel cc on cc.contractid = c.ContractID "
        SQL = SQL + "where c.status = 'Cancelled' "
        SQL = SQL + "and cc.CancelStatus = 'Cancelled' "
        SQL = SQL + "and saledate < '" & sEndDate & "' "
        SQL = SQL + "and cc.CancelDate >= '" & sStatDate & "' "
        SQL = SQL + "and c.an <> 0 "
        SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,103,105)"
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.claimpercentage "
            SQL = SQL + "set contractcnt = contractcnt + " & clC.Fields("cnt") & " "
            SQL = SQL + "where type = 'AN' "
            SQL = SQL + "and statmonth = '" & sStatDate & "' "
            clR.RunSQL(SQL, sCON)
        End If

    End Sub

    Private Sub CalcContractCancel()
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select count(distinct c.contractid) as cnt from contract c "
        SQL = SQL + "inner join contractcancel cc on cc.contractid = c.ContractID "
        SQL = SQL + "where c.status = 'Cancelled' "
        SQL = SQL + "and cc.CancelStatus = 'Cancelled' "
        SQL = SQL + "and saledate < '" & sEndDate & "' "
        SQL = SQL + "and cc.CancelDate >= '" & sStatDate & "' "
        SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,103,105)"
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.claimpercentage "
            SQL = SQL + "set contractcnt = contractcnt + " & clC.Fields("cnt") & " "
            SQL = SQL + "where type = 'All' "
            SQL = SQL + "and statmonth = '" & sStatDate & "' "
            clR.RunSQL(SQL, sCON)
        End If

    End Sub

    Private Sub CalcContractPendingPaidAN()
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where saledate < '" & sEndDate & "' "
        SQL = SQL + "and (status = 'Pending' "
        SQL = SQL + "or status = 'Paid') "
        SQL = SQL + "and an <> 0 "
        SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,103,105)"
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.claimpercentage "
            SQL = SQL + "set contractcnt = contractcnt + " & clC.Fields("cnt") & " "
            SQL = SQL + "where type = 'AN' "
            SQL = SQL + "and statmonth = '" & sStatDate & "' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub CalcContractPendingPaid()
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where saledate < '" & sEndDate & "' "
        SQL = SQL + "and (status = 'Pending' "
        SQL = SQL + "or status = 'Paid') "
        SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,103,105)"
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.claimpercentage "
            SQL = SQL + "set contractcnt = contractcnt + " & clC.Fields("cnt") & " "
            SQL = SQL + "where type = 'All' "
            SQL = SQL + "and statmonth = '" & sStatDate & "' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub
End Module
