﻿Module mPaidClaim
    Private sDate As String
    Private sStartDate As String
    Private sEndDate As String

    Public Sub PaidClaim()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.processedpayments "
        clR.RunSQL(SQL, sCON)
        sDate = Month(Today) & "/1/" & Year(Today) - 1
        For cnt = 0 To 12
            sStartDate = DateAdd(DateInterval.Month, cnt, CDate(sDate))
            sEndDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
            InsertRows()
            CalcEPClaim()
            CalcANClaim()
            CalcOtherClaim()
        Next
    End Sub

    Private Sub CalcOtherClaim()
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select sum(cd.TotalAmt) as amt from claim cl 
        inner join claimdetail cd on cl.ClaimID = cd.ClaimID
        inner join contract c on cl.ContractID = c.ContractID
        where an = 0
        and not contractno like 'vep%'
        and not contractno like 'rep%'
        and cd.ClaimDetailStatus = 'Paid' "
        SQL = SQL + "and cd.datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and cd.datepaid < '" & sEndDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.processedpayments "
            SQL = SQL + "set amt = " & clC.Fields("amt") & " "
            SQL = SQL + "where type = 'No EP/AN' "
            SQL = SQL + "and statdate = '" & sStartDate & "' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub CalcANClaim()
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select sum(cd.TotalAmt) as amt from claim cl 
        inner join claimdetail cd on cl.ClaimID = cd.ClaimID
        inner join contract c on cl.ContractID = c.ContractID
        where an <> 0
        and cd.ClaimDetailStatus = 'Paid' "
        SQL = SQL + "and cd.datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and cd.datepaid < '" & sEndDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.processedpayments "
            SQL = SQL + "set amt = " & clC.Fields("amt") & " "
            SQL = SQL + "where type = 'AN' "
            SQL = SQL + "and statdate = '" & sStartDate & "' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub CalcEPClaim()
        Dim clR As New clsDBO
        Dim clC As New clsDBO
        SQL = "select sum(cd.TotalAmt) as amt from claim cl 
        inner join claimdetail cd on cl.ClaimID = cd.ClaimID
        inner join contract c on cl.ContractID = c.ContractID
        where (contractno like 'vep%'
        or contractno like 'rep%')
        and cd.ClaimDetailStatus = 'Paid' "
        SQL = SQL + "and cd.datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and cd.datepaid < '" & sEndDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.processedpayments "
            SQL = SQL + "set amt = " & clC.Fields("amt") & " "
            SQL = SQL + "where type = 'EP' "
            SQL = SQL + "and statdate = '" & sStartDate & "' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub InsertRows()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.processedpayments "
        SQL = SQL + "(type, statdate, amt) "
        SQL = SQL + "values ('EP', '" & sStartDate & "', 0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.processedpayments "
        SQL = SQL + "(type, statdate, amt) "
        SQL = SQL + "values ('AN', '" & sStartDate & "', 0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.processedpayments "
        SQL = SQL + "(type, statdate, amt) "
        SQL = SQL + "values ('No EP/AN', '" & sStartDate & "', 0)"
        clR.RunSQL(SQL, sCON)
    End Sub

End Module
