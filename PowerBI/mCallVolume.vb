﻿Module mCallVolume
    Private sDate As String
    Private sStartDate As String
    Private sEndDate As String

    Public Sub CallVolume()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.MonthCallCountV2 "
        clR.RunSQL(SQL, sCON)

        Dim monthInt = Month(Today)
        Dim yearInt = Year(Today)
        Dim sYearEndDate As String

        If monthInt <= 8 And yearInt = 2022 Then
            sStartDate = "9/1/" + CStr(yearInt - 1)
            sEndDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
            sYearEndDate = CStr(monthInt + 1) + "/1/" + CStr(yearInt)
            While CDate(sStartDate) < CDate(sYearEndDate)
                InsertRows()
                GetCallCount()
                GetMonthlyTotal()
                sStartDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
                sEndDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
            End While
        Else
            sDate = monthInt & "/1/" & (yearInt - 1)
            For cnt = 0 To 12
                sStartDate = DateAdd(DateInterval.Month, cnt, CDate(sDate))
                sEndDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
                InsertRows()
                GetCallCount()
                GetMonthlyTotal()
            Next
        End If
        CalcRunRate()
    End Sub

    Private Sub CalcRunRate()
        Dim sMonth As String
        Dim lDay As Long
        Dim lTotalDays As Long
        Dim clR As New clsDBO
        Dim clM As New clsDBO
        Dim lTemp As Long
        sMonth = Month(Today) & "/1/" & Year(Today)
        lDay = Day(Today)
        lDay = lDay - 1
        lTotalDays = DateDiff(DateInterval.Day, CDate(sMonth), DateAdd(DateInterval.Month, 1, CDate(sMonth)))
        SQL = "select * from veritaspowerbi.dbo.MonthCallCountV2 "
        SQL = SQL + "where callmonth = '" & sMonth & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from veritaspowerbi.dbo.MonthCallCountV2 "
                SQL = SQL + "where idx = " & clR.Fields("idx")
                clM.OpenDB(SQL, sCON)
                If clM.RowCount > 0 Then
                    clM.GetRow()
                    If CLng(clM.Fields("outbound")) > 0 Then
                        lTemp = (CLng(clM.Fields("outbound")) / lDay) * lTotalDays
                        clM.Fields("outbound") = lTemp
                    End If
                    If CLng(clM.Fields("inbound")) > 0 Then
                        lTemp = (clM.Fields("inbound") / lDay) * lTotalDays
                        clM.Fields("inbound") = lTemp
                    End If
                    clM.SaveDB()
                End If
            Next
        End If
    End Sub

    Private Sub GetMonthlyTotal()
        Dim clC As New clsDBO
        Dim clR As New clsDBO
        SQL = "select sum(OutBound) as OutBoundTotal, sum(InBound) as InBoundTotal from VeritasPowerBI.dbo.MonthCallCountV2 " +
              "where callmonth = '" & sStartDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.MonthCallCountV2 "
            SQL = SQL + "set outbound = " & clC.Fields("OutBoundTotal") & ", "
            SQL = SQL + "inbound = " & clC.Fields("InBoundTotal") & " "
            SQL = SQL + "where callmonth = '" & sStartDate & "' "
            SQL = SQL + "and calldept = 'Total' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub GetCallCount()
        Dim clC As New clsDBO
        Dim clR As New clsDBO
        Dim DeptName = ""
        SQL = "select 'Claim' as TeamName, sum(inboundct) as INC, sum(outboundct) obc from VeritasPhone.dbo.AgentDailyCallSummary ac " +
              "Left join veritas.dbo.UserSecurityInfo usi on usi.UserID = ac.UserID " +
              "left join veritas.dbo.UserTeam ut on ut.TeamID = usi.TeamID " +
              "where calldate >= '" & sStartDate & "' " +
              "and calldate < '" & sEndDate & "' " +
              "and ut.TeamID in (1,2,3,4,5,11) " +
              "Union " +
              "select TeamName, sum(inboundct) as INC, sum(outboundct) obc from VeritasPhone.dbo.AgentDailyCallSummary ac " +
              "left join veritas.dbo.UserSecurityInfo usi on usi.UserID = ac.UserID " +
              "left join veritas.dbo.UserTeam ut on ut.TeamID = usi.TeamID " +
              "where calldate >= '" & sStartDate & "' " +
              "and calldate < '" & sEndDate & "' " +
              "and ut.TeamID in (7,8,9) " +
              "group by TeamName " +
              "order by TeamName "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            For cnt = 0 To clC.RowCount - 1
                If cnt = 0 Then
                    DeptName = "Payments"
                ElseIf cnt = 1 Then
                    DeptName = "Claims"
                ElseIf cnt = 2 Then
                    DeptName = "CS"
                ElseIf cnt = 3 Then
                    DeptName = "Dealer Services"
                End If
                clC.GetRowNo(cnt)
                SQL = "update veritaspowerbi.dbo.MonthCallCountV2 "
                SQL = SQL + "set outbound = " & clC.Fields("obc") & ", "
                SQL = SQL + "inbound = " & clC.Fields("inc") & " "
                SQL = SQL + "where callmonth = '" & sStartDate & "' "
                SQL = SQL + "and calldept = '" & DeptName & "' "
                clR.RunSQL(SQL, sCON)
            Next
        End If
    End Sub

    Private Sub InsertRows()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.MonthCallCountV2 "
        SQL = SQL + "(calldept, callmonth, outbound, inbound) "
        SQL = SQL + "values ('Claims', '" & sStartDate & "', 0,0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.MonthCallCountV2 "
        SQL = SQL + "(calldept, callmonth, outbound, inbound) "
        SQL = SQL + "values ('CS', '" & sStartDate & "', 0,0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.MonthCallCountV2 "
        SQL = SQL + "(calldept, callmonth, outbound, inbound) "
        SQL = SQL + "values ('Dealer Services', '" & sStartDate & "', 0,0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.MonthCallCountV2 "
        SQL = SQL + "(calldept, callmonth, outbound, inbound) "
        SQL = SQL + "values ('Payments', '" & sStartDate & "', 0,0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.MonthCallCountV2 "
        SQL = SQL + "(calldept, callmonth, outbound, inbound) "
        SQL = SQL + "values ('Total', '" & sStartDate & "', 0,0)"
        clR.RunSQL(SQL, sCON)
    End Sub

End Module
