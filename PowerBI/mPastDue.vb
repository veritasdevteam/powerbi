﻿Module mPastDue
    Private sStartDate As String
    Private sEndDate As String
    Private l3059 As Long = 0
    Private l3044 As Long = 0
    Private l4559 As Long = 0
    Private l6089 As Long = 0
    Private l90119 As Long = 0
    Private l120 As Long = 0

    Public Sub PastDue()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.pastduecount "
        clR.RunSQL(SQL, sCON)
        sStartDate = DateAdd(DateInterval.Month, -3, Today)
        sEndDate = DateAdd(DateInterval.Day, 6, CDate(sStartDate))
        Do Until CDate(sStartDate) >= Today
            CalcPending3059()
            CalcPaid3059()
            CalcPending3044()
            CalcPaid3044()
            CalcPending4559()
            CalcPaid4559()
            CalcPending6089()
            CalcPending6089()
            CalcPaid6089()
            CalcPending90119()
            CalcPaid90119()
            CalcPending120()
            CalcPaid120()
            AddToDB()
            sStartDate = DateAdd(DateInterval.Day, 1, CDate(sEndDate))
            sEndDate = DateAdd(DateInterval.Day, 6, CDate(sStartDate))
        Loop
    End Sub

    Private Sub AddToDB()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.pastduecount "
        clR.OpenDB(SQL, sCON)
        clR.NewRow()
        clR.Fields("weekdate") = sStartDate
        clR.Fields("day3059") = l3059
        clR.Fields("day3044") = l3044
        clR.Fields("day4559") = l4559
        clR.Fields("day6089") = l6089
        clR.Fields("day90119") = l90119
        clR.Fields("dayover120") = l120
        clR.AddRow()
        clR.SaveDB()
    End Sub

    Private Sub CalcPaid120()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'paid' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and saledate <= '" & sEndDate & "' "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 119 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 180"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l90119 = l90119 + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcPaid90119()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'paid' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and saledate <= '" & sEndDate & "' "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 89 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 120 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l90119 = l90119 + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcPaid6089()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'paid' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and saledate <= '" & sEndDate & "' "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 60 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 89 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l6089 = l6089 + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcPaid4559()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'paid' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and saledate <= '" & sEndDate & "' "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 44 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 60 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l4559 = l4559 + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcPaid3044()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'paid' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and saledate <= '" & sEndDate & "' "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 29 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 45 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l3044 = l3044 + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcPaid3059()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'paid' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and saledate <= '" & sEndDate & "' "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 29 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 60 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l3059 = l3059 + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcPending120()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'Pending' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 119 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 180"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l120 = clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcPending90119()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'Pending' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 89 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 120 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l90119 = clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcPending6089()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'Pending' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 59 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 90 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l6089 = clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcPending4559()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'Pending' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 44 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 60 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l4559 = clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcPending3044()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'Pending' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 29 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 45 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l3044 = clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcPending3059()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where status = 'Pending' "
        SQL = SQL + "and an = 0 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') > 29 "
        SQL = SQL + "and datediff(day, saledate, '" & sEndDate & "') < 60 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount() > 0 Then
            clR.GetRow()
            l3059 = clR.Fields("cnt")
        End If
    End Sub

End Module
