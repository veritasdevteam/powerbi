﻿Module mWexBiWeekly

    Dim sStartDate As String
    Dim sEndDate As String
    Dim sWeekDate As String
    Dim SQL As String

    Public Sub WexBiWeekly()
        Dim clR As New clsDBO
        ClearWeek()
        Dim lDay As Long = 0
        Dim cnt As Double = 0
        Dim dStartDate As String = ""
        Dim dEndDate As String = ""
        Dim SQL As String = ""
        Dim dtMin As Date = New Date(2020, 3, 4)
        Dim A1 As Date = Today
        Dim A2 As Date = New Date(2020, 3, 4)
        Dim dtStartDate As Date = New Date
        Dim DiffResult As TimeSpan = CType(A1 - A2, TimeSpan)
        lDay = DiffResult.Days

        Do
            dtStartDate = A2.AddDays(cnt).AddDays(-13)
            If (dtStartDate < dtMin) Then
                dtStartDate = dtMin
            End If
            sEndDate = A2.AddDays(cnt).ToString()
            sStartDate = dtStartDate.ToString()

            SQL = "insert into veritaspowerbi.dbo.claimwexbiweekly " +
                  "(paiddate, dailytype, Amt) " +
                  "select '" + sEndDate + "', " +
                  "'All', " +
                  "avg(paidamt) " +
                  "from (" +
                  "select  sum(cd.paidamt) as PaidAmt " +
                  "from Claimdetail cd " +
                  "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                  "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                  "where cd.datepaid >= '" + sStartDate + "' " +
                  "and cd.datepaid <= '" + sEndDate + "' " +
                  "and not cp.DateTransmitted is null " +
                  "and not cp.wexcode is null " +
                  "and cd.ClaimDetailStatus = 'Paid' " +
                  "and paidamt <> 0 " +
                  "group by ClaimID) aa"
            clR.RunSQL(SQL, sCON)

            SQL = "insert into veritaspowerbi.dbo.claimwexbiweekly " +
                  "(paiddate, dailytype, Amt) " +
                  "select '" + sEndDate + "', " +
                  "'All No Ancillary', " +
                  "avg(paidamt) " +
                  "from (" +
                  "select  sum(cd.paidamt) as PaidAmt " +
                  "from Claimdetail cd " +
                  "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                  "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                  "inner join claim cl on cl.claimid = cd.claimid " +
                  "inner join contract c on c.contractid = cl.contractid " +
                  "where cd.datepaid >= '" + sStartDate + "' " +
                  "and cd.datepaid <= '" + sEndDate + "' " +
                  "and not cp.DateTransmitted is null " +
                  "and cd.ClaimDetailStatus = 'Paid' " +
                  "and paidamt <> 0 " +
                  "and not c.programid in (67,103,105) " +
                  "group by cd.ClaimID) aa"
            clR.RunSQL(SQL, sCON)

            SQL = "insert into veritaspowerbi.dbo.claimwexbiweekly " +
                  "(paiddate, dailytype, Amt) " +
                  "select '" + sEndDate + "', " +
                  "'All No AN', " +
                  "case when avg(paidamt) is null then 0 else avg(paidamt) end " +
                  "from (" +
                  "select  sum(cd.paidamt) as PaidAmt " +
                  "from Claimdetail cd " +
                  "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                  "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                  "inner join claim cl on cl.claimid = cd.claimid " +
                  "inner join contract c on c.contractid = cl.contractid " +
                  "where cd.datepaid >= '" + sStartDate + "' " +
                  "and cd.datepaid <= '" + sEndDate + "' " +
                  "and not cp.DateTransmitted is null and cd.ClaimDetailStatus = 'Paid' " +
                  "and paidamt <> 0 " +
                  "and an = 0 " +
                  "group by cd.ClaimID) aa"
            clR.RunSQL(SQL, sCON)

            SQL = "insert into veritaspowerbi.dbo.claimwexbiweekly " +
                  "(paiddate, dailytype, Amt) " +
                  "select '" + sEndDate + "', " +
                  "'All no Ancillary/AN', " +
                  "case when avg(paidamt) is null then 0 else avg(paidamt) end " +
                  "from (" +
                  "select  sum(cd.paidamt) as PaidAmt " +
                  "from Claimdetail cd " +
                  "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                  "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                  "inner join claim cl on cl.claimid = cd.claimid " +
                  "inner join contract c on c.contractid = cl.contractid " +
                  "where cd.datepaid >= '" + sStartDate + "' " +
                  "and cd.datepaid <= '" + sEndDate + "' " +
                  "and not cp.DateTransmitted is null and cd.ClaimDetailStatus = 'Paid' " +
                  "and paidamt <> 0 " +
                  "and not c.programid in (67,103,105) " +
                  "and an = 0 " +
                  "group by cd.ClaimID) aa"
            clR.RunSQL(SQL, sCON)

            cnt += 1
        Loop While (cnt < lDay)

    End Sub

    Private Sub ClearWeek()
        Dim clR As New clsDBO
        Dim SQL As String = ""
        SQL = "truncate table veritaspowerbi.dbo.claimwexbiweekly "
        clR.RunSQL(SQL, sCON)
    End Sub
End Module
