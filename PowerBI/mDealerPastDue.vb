﻿Module mDealerPastDue

    Private sYearDate As String
    Private sCurWeekStart As String
    Private sCurWeekEnd As String
    Private sPrevWeekStart As String
    Private sPrevWeekEnd As String
    Private lDealerID As Long
    Private sType As String
    Private lTotal As Long
    Private l3059 As Long
    Private l3044 As Long
    Private l4559 As Long
    Private l6089 As Long
    Private l90119 As Long
    Private l120 As Long
    Private l30Prev As Long
    Private lTotalPrev As Long
    Private lTotal30Prev As Long
    Private l3059Prev As Long
    Private l3044Prev As Long
    Private l4559Prev As Long
    Private l6089Prev As Long
    Private l90119Prev As Long
    Private l120Prev As Long
    Private lTotalDiff As Long
    Private l3059Diff As Long
    Private l3044Diff As Long
    Private l4559Diff As Long
    Private l6089Diff As Long
    Private l90119Diff As Long
    Private l120Diff As Long

    Public Sub DealerPastDue()
        Dim clR As New clsDBO
        sCurWeekEnd = Today
        sCurWeekStart = DateAdd(DateInterval.Day, -7, CDate(sCurWeekEnd))
        sPrevWeekEnd = sCurWeekStart
        sPrevWeekStart = DateAdd(DateInterval.Day, -7, CDate(sPrevWeekEnd))
        sYearDate = DateAdd(DateInterval.Year, -1, Today)
        cleardealerpastdue
        SQL = "select top 10 dealerid, count(*) as cnt from contract "
        SQL = SQL + "where status = 'pending '"
        SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and dealerid <> 0 "
        SQL = SQL + "and not contractno like 'rac%'"
        SQL = SQL + "group by dealerid "
        SQL = SQL + "order by count(*) desc "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                lTotal = 0
                l3059 = 0
                l3044 = 0
                l4559 = 0
                l6089 = 0
                l90119 = 0
                l120 = 0
                lDealerID = clR.Fields("dealerid")
                sType = "Current"
                lTotal = clR.Fields("cnt")
                CalcCur3059()
                CalcCur3044()
                CalcCur4559()
                CalcCur6089()
                CalcCur90119()
                CalcCur120()
                InsertIntoDealerPastDue()
                l30Prev = 0
                l3059Prev = 0
                l3044Prev = 0
                l4559Prev = 0
                l6089Prev = 0
                l90119Prev = 0
                l120Prev = 0
                CalcPrevTotal30()
                CalcPrev3059()
                CalcPrev3044()
                CalcPrev4559()
                CalcPrev6089()
                CalcPrev90119()
                CalcPrev120()
                lTotalPrev = l30Prev + l3059Prev + l6089Prev + l90119Prev + l120Prev
                InsertIntoDealerPastDuePrev()
                lTotalDiff = lTotal - lTotalPrev
                l3044Diff = l3044 - l3044Prev
                l3059Diff = l3059 - l3059Prev
                l4559Diff = l4559 - l4559Prev
                l6089Diff = l6089 - l6089Prev
                l90119Diff = l90119 - l90119Prev
                l120Diff = l120 - l120Prev
                insertintodealerpastduediff
            Next
        End If
    End Sub

    Private Sub InsertIntoDealerPastDueDiff()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.dealerpastdue "
        SQL = SQL + "(dealerid, type, total, days3059, days3044, days4559, days6089, days90119, daysover120) "
        SQL = SQL + "values (" & lDealerID & ", "
        SQL = SQL + "'Difference', "
        SQL = SQL & lTotalDiff & ", "
        SQL = SQL & l3059Diff & ", "
        SQL = SQL & l3044Diff & ", "
        SQL = SQL & l4559Diff & ", "
        SQL = SQL & l6089Diff & ", "
        SQL = SQL & l90119Diff & ", "
        SQL = SQL & l120Diff & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub ClearDealerPastDue()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.dealerpastdue "
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub InsertIntoDealerPastDuePrev()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.dealerpastdue "
        SQL = SQL + "(dealerid, type, total, days3059, days3044, days4559, days6089, days90119, daysover120) "
        SQL = SQL + "values (" & lDealerID & ", "
        SQL = SQL + "'Previous', "
        SQL = SQL & lTotalPrev & ", "
        SQL = SQL & l3059Prev & ", "
        SQL = SQL & l3044Prev & ", "
        SQL = SQL & l4559Prev & ", "
        SQL = SQL & l6089Prev & ", "
        SQL = SQL & l90119Prev & ", "
        SQL = SQL & l120Prev & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub InsertIntoDealerPastDue()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.dealerpastdue "
        SQL = SQL + "(dealerid, type, total, days3059, days3044, days4559, days6089, days90119, daysover120) "
        SQL = SQL + "values (" & lDealerID & ", "
        SQL = SQL + "'Current', "
        SQL = SQL & lTotal & ", "
        SQL = SQL & l3059 & ", "
        SQL = SQL & l3044 & ", "
        SQL = SQL & l4559 & ", "
        SQL = SQL & l6089 & ", "
        SQL = SQL & l90119 & ", "
        SQL = SQL & l120 & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcPrev120()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -119, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        SQL = SQL + "and status = 'pending '"

        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l120Prev = clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'paid' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -119, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        SQL = SQL + "and datepaid >= '" & DateAdd(DateInterval.Day, -119, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datediff(d, saledate, datepaid) > 119"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l120Prev = l120Prev + clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract c "
        SQL = SQL + "inner join contractcancel cc on cc.contractid = c.contractid "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status like 'cancel%' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -119, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and datepaid is null "
        SQL = SQL + "and canceldate >= '" & DateAdd(DateInterval.Day, -119, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datediff(d, saledate, canceldate) > 119"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l120Prev = l120Prev + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcPrev6089()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -89, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        SQL = SQL + "and status = 'pending '"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l6089Prev = clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'paid' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -89, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid >= '" & DateAdd(DateInterval.Day, -89, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid < '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l6089Prev = l6089Prev + clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract c "
        SQL = SQL + "inner join contractcancel cc on cc.contractid = c.contractid "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status like 'cancel%' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -89, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid is null "
        SQL = SQL + "and canceldate >= '" & DateAdd(DateInterval.Day, -89, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and canceldate < '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l6089Prev = l6089Prev + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcPrev90119()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -119, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -89, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and status = 'pending '"
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l90119Prev = clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'paid' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -119, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -89, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid >= '" & DateAdd(DateInterval.Day, -119, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid < '" & DateAdd(DateInterval.Day, -89, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l90119Prev = l90119Prev + clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract c "
        SQL = SQL + "inner join contractcancel cc on cc.contractid = c.contractid "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status like 'cancel%' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -119, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -89, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and datepaid is null "
        SQL = SQL + "and canceldate >= '" & DateAdd(DateInterval.Day, -119, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and canceldate < '" & DateAdd(DateInterval.Day, -89, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l90119Prev = l90119Prev + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcPrev4559()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -44, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and status = 'pending '"
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l4559Prev = clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'paid' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -44, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid >= '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid < '" & DateAdd(DateInterval.Day, -44, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l4559Prev = l4559Prev + clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract c "
        SQL = SQL + "inner join contractcancel cc on cc.contractid = c.contractid "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status like 'cancel%' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -44, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid is null "
        SQL = SQL + "and canceldate >= '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and canceldate < '" & DateAdd(DateInterval.Day, -44, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l4559Prev = l4559Prev + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcPrev3044()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -44, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -29, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and status = 'pending '"
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l3044Prev = clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'paid' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -44, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -29, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid >= '" & DateAdd(DateInterval.Day, -44, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid < '" & DateAdd(DateInterval.Day, -29, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l3044Prev = l3044Prev + clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract c "
        SQL = SQL + "inner join contractcancel cc on cc.contractid = c.contractid "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status like 'cancel%' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -44, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -29, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid is null "
        SQL = SQL + "and canceldate >= '" & DateAdd(DateInterval.Day, -44, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and canceldate < '" & DateAdd(DateInterval.Day, -29, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l3044Prev = l3044Prev + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcPrevTotal30()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and saledate > '" & DateAdd(DateInterval.Day, -30, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & sPrevWeekEnd & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and status = 'pending '"
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lTotal30Prev = clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'paid' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -30, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & sPrevWeekEnd & "' "
        SQL = SQL + "and datepaid >= '" & DateAdd(DateInterval.Day, -30, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid < '" & sPrevWeekEnd & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lTotal30Prev = lTotal30Prev + clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract c "
        SQL = SQL + "inner join contractcancel cc on cc.contractid = c.contractid "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status like 'cancel%' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -30, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & sPrevWeekEnd & "' "
        SQL = SQL + "and datepaid is null "
        SQL = SQL + "and canceldate >= '" & DateAdd(DateInterval.Day, -30, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and canceldate < '" & sPrevWeekEnd & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lTotal30Prev = lTotal30Prev + clR.Fields("cnt")
            End If
        End If

    End Sub

    Private Sub CalcPrev3059()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'Pending' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -29, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l3059 = clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'paid' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -29, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid >= '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid < '" & DateAdd(DateInterval.Day, -29, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l3059 = l3059 + clR.Fields("cnt")
            End If
        End If
        SQL = "select count(*) as cnt from contract c "
        SQL = SQL + "inner join contractcancel cc on cc.contractid = c.contractid "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status like 'cancel%' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -29, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and datepaid is null "
        SQL = SQL + "and canceldate >= '" & DateAdd(DateInterval.Day, -59, CDate(sPrevWeekEnd)) & "' "
        SQL = SQL + "and canceldate < '" & DateAdd(DateInterval.Day, -29, CDate(sPrevWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l3059 = l3059 + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcCur120()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'Pending' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -119, CDate(sCurWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l120 = clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcCur90119()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'Pending' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -119, CDate(sCurWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -89, CDate(sCurWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l90119 = clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcCur6089()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'Pending' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -89, CDate(sCurWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -58, CDate(sCurWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l6089 = clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcCur4559()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'Pending' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -59, CDate(sCurWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -44, CDate(sCurWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l4559 = clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcCur3044()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'Pending' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -44, CDate(sCurWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -29, CDate(sCurWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l3044 = clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcCur3059()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where dealerid = " & lDealerID & " "
        SQL = SQL + "and status = 'Pending' "
        SQL = SQL + "and saledate >= '" & DateAdd(DateInterval.Day, -59, CDate(sCurWeekEnd)) & "' "
        SQL = SQL + "and saledate < '" & DateAdd(DateInterval.Day, -29, CDate(sCurWeekEnd)) & "' "
        'SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,65,67,101,103,105) "
        SQL = SQL + "and not contractno like 'rac%'"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                l3059 = clR.Fields("cnt")
            End If
        End If
    End Sub

End Module
