﻿Module mContractMonthCount
    Private clC As New clsDBO
    Private lAllContractCnt As Long
    Private lRedContractCnt As Long
    Private lVeritasContractCnt As Long
    Private lProductContractCnt As Long
    Private lVSCContractCnt As Long
    Private lANContractCnt As Long
    Private lNonANContractCnt As Long
    Private sStartDate2 As String
    Private sEndDate2 As String

    Public Sub ContractMonthCount()
        SQL = "truncate table VeritasPowerBI.dbo.contractmonthcount "
        clC.RunSQL(SQL, sCON)
        Dim sStartDate As String = "1/1/2020"
        Dim sEndDate As String
        Dim lMonth As Long
        sEndDate = Month(Today) & "/1/" & Year(Today)
        lMonth = DateDiff(DateInterval.Month, CDate(sStartDate), CDate(sEndDate))
        For cntMonth = 0 To lMonth
            lAllContractCnt = 0
            lRedContractCnt = 0
            lVeritasContractCnt = 0
            lProductContractCnt = 0
            lVSCContractCnt = 0
            lANContractCnt = 0
            lNonANContractCnt = 0
            sStartDate2 = DateAdd(DateInterval.Month, cntMonth, CDate(sStartDate))
            sEndDate2 = DateAdd(DateInterval.Month, 1, CDate(sStartDate2))
            GetAllCancel()
            GetAllExpired()
            GetAllPaid()
            InsertAll()
            GetRedCancel()
            GetRedExpired()
            GetRedPaid()
            InsertRed()
            GetVeritasCancel()
            GetVeritasExpired()
            GetVeritasPaid()
            InsertVeritas()
            GetProductCancel()
            GetProductExpired()
            GetProductPaid()
            InsertProduct()
            GetVSCCancel()
            GetVSCPaid()
            GetVSCExpired()
            InsertVSC()
            GetANCancel()
            GetANExpired()
            GetANPaid()
            InsertAN()
            GetNonANCancel()
            GetNonANExpired()
            GetNonANPaid()
            InsertNonAN()
        Next

    End Sub

    Private Sub CalcRunRate()
        Dim clR As New clsDBO
        Dim clM As New clsDBO
        Dim sMonth As String
        Dim sMonthEnd As String
        Dim lDays As Long
        Dim lDaysTotal As Long
        sMonth = Month(Today) & "/1/" & Year(Today)
        sMonthEnd = DateAdd(DateInterval.Month, 1, CDate(sMonth))
        lDaysTotal = DateDiff(DateInterval.Day, CDate(sMonth), CDate(sMonthEnd))
        lDays = DateDiff(DateInterval.Day, CDate(sMonth), Today)
        lDays = lDays - 1
        SQL = "select * from VeritasPowerBI.dbo.contractmonthcount "
        SQL = SQL + "where month = '" & Month(Today) & "/1/" & Year(Today) & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from VeritasPowerBI.dbo.contractmonthcount "
                SQL = SQL + "where idx = " & clR.Fields("idx")
                clM.OpenDB(SQL, sCON)
                If clM.RowCount > 0 Then
                    clM.GetRow()
                    clM.Fields("count") = CLng((CLng(clM.Fields("count")) / lDays) * lDaysTotal)
                    clM.SaveDB()
                End If
            Next
        End If
    End Sub

    Private Sub InsertNonAN()
        Dim clR As New clsDBO
        SQL = "insert into VeritasPowerBI.dbo.contractmonthcount "
        SQL = SQL + "(contracttype, month, count) "
        SQL = SQL + "values ('Non-AN', '" & sStartDate2 & "', " & lNonANContractCnt & ")"
        clR.RunSQL(SQL, sCon)
    End Sub

    Private Sub InsertAN()
        Dim clR As New clsDBO
        SQL = "insert into VeritasPowerBI.dbo.contractmonthcount "
        SQL = SQL + "(contracttype, month, count) "
        SQL = SQL + "values ('AN', '" & sStartDate2 & "', " & lANContractCnt & ")"
        clR.RunSQL(SQL, sCon)
    End Sub

    Private Sub InsertVSC()
        Dim clR As New clsDBO
        SQL = "insert into VeritasPowerBI.dbo.contractmonthcount "
        SQL = SQL + "(contracttype, month, count) "
        SQL = SQL + "values ('VSC', '" & sStartDate2 & "', " & lVSCContractCnt & ")"
        clR.RunSQL(SQL, sCon)
    End Sub

    Private Sub InsertProduct()
        Dim clR As New clsDBO
        SQL = "insert into VeritasPowerBI.dbo.contractmonthcount "
        SQL = SQL + "(contracttype, month, count) "
        SQL = SQL + "values ('Product', '" & sStartDate2 & "', " & lProductContractCnt & ")"
        clR.RunSQL(SQL, sCon)
    End Sub

    Private Sub InsertVeritas()
        Dim clR As New clsDBO
        SQL = "insert into VeritasPowerBI.dbo.contractmonthcount "
        SQL = SQL + "(contracttype, month, count) "
        SQL = SQL + "values ('Veritas', '" & sStartDate2 & "', " & lVeritasContractCnt & ")"
        clR.RunSQL(SQL, sCon)
    End Sub

    Private Sub InsertRed()
        Dim clR As New clsDBO
        SQL = "insert into VeritasPowerBI.dbo.contractmonthcount "
        SQL = SQL + "(contracttype, month, count) "
        SQL = SQL + "values ('Red Auto', '" & sStartDate2 & "', " & lRedContractCnt & ")"
        clR.RunSQL(SQL, sCon)
    End Sub

    Private Sub InsertAll()
        Dim clR As New clsDBO
        SQL = "insert into VeritasPowerBI.dbo.contractmonthcount "
        SQL = SQL + "(contracttype, month, count) "
        SQL = SQL + "values ('All', '" & sStartDate2 & "', " & lAllContractCnt & ")"
        clR.RunSQL(SQL, sCon)
    End Sub

    Private Sub GetNonANPaid()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Paid' "
        SQL = SQL + "and not contractno like '%AN%' "
        SQL = SQL + "and not programid in (47,48,51,55,56,57,58,60,67,103,105) "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lNonANContractCnt = lNonANContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetANPaid()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Paid' "
        SQL = SQL + "and contractno like '%AN%' "
        SQL = SQL + "and not programid in (47,48,51,55,56,57,58,60,67,103,105) "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lANContractCnt = lANContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetVSCPaid()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Paid' "
        SQL = SQL + "and not programid in (47,48,51,55,56,57,58,60,67,103,105) "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lVSCContractCnt = lVSCContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetProductPaid()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Paid' "
        SQL = SQL + "and programid in (47,48,51,55,56,57,58,60,67,103,105) "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lProductContractCnt = lProductContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetVeritasPaid()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Paid' "
        SQL = SQL + "and programname = 'Veritas' "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lVeritasContractCnt = lVeritasContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetRedPaid()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Paid' "
        SQL = SQL + "and programname = 'Red Auto' "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lRedContractCnt = lRedContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetAllPaid()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Paid' "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lAllContractCnt = lAllContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetNonANExpired()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Expired' "
        SQL = SQL + "and not contractno like '%AN%' "
        SQL = SQL + "and not programid in (47,48,51,55,56,57,58,60,67,103,105) "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and expdate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lNonANContractCnt = lNonANContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetANExpired()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Expired' "
        SQL = SQL + "and contractno like '%AN%' "
        SQL = SQL + "and not programid in (47,48,51,55,56,57,58,60,67,103,105) "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and expdate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lANContractCnt = lANContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetVSCExpired()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Expired' "
        SQL = SQL + "and not programid in (47,48,51,55,56,57,58,60,67,103,105) "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and expdate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lVSCContractCnt = lVSCContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetProductExpired()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Expired' "
        SQL = SQL + "and programid in (47,48,51,55,56,57,58,60,67,103,105) "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and expdate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lProductContractCnt = lProductContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetVeritasExpired()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Expired' "
        SQL = SQL + "and programname = 'Veritas' "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and expdate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lVeritasContractCnt = lVeritasContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetRedExpired()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Expired' "
        SQL = SQL + "and programname = 'Red Auto' "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and expdate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lRedContractCnt = lRedContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetAllExpired()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c "
        SQL = SQL + "where status = 'Expired' "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and expdate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lAllContractCnt = lAllContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetNonANCancel()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c
        inner join veritas.dbo.ContractCancel cc on cc.contractid = c.ContractID
        where status = 'cancelled'
        and not contractno like '%AN%'
        and not programid in (47,48,51,55,56,57,58,60,67,103,105)
        and not datepaid is null "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and canceldate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lNonANContractCnt = lNonANContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetANCancel()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c
        inner join veritas.dbo.ContractCancel cc on cc.contractid = c.ContractID
        where status = 'cancelled'
        and contractno like '%AN%'
        and not programid in (47,48,51,55,56,57,58,60,67,103,105)
        and not datepaid is null "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and canceldate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lANContractCnt = lANContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetVSCCancel()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c
        inner join veritas.dbo.ContractCancel cc on cc.contractid = c.ContractID
        where status = 'cancelled'
        and not programid in (47,48,51,55,56,57,58,60,67,103,105)
        and not datepaid is null "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and canceldate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lVSCContractCnt = lVSCContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetProductCancel()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c
        inner join veritas.dbo.ContractCancel cc on cc.contractid = c.ContractID
        where status = 'cancelled'
        and programid in (47,48,51,55,56,57,58,60,67,103,105)
        and not datepaid is null "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and canceldate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lProductContractCnt = lProductContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetVeritasCancel()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c
        inner join veritas.dbo.ContractCancel cc on cc.contractid = c.ContractID
        where status = 'cancelled'
        and programname = 'Veritas'
        and not datepaid is null "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and canceldate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lVeritasContractCnt = lVeritasContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetRedCancel()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c
        inner join veritas.dbo.ContractCancel cc on cc.contractid = c.ContractID
        where status = 'cancelled'
        and programname = 'Red Auto'
        and not datepaid is null "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and canceldate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lRedContractCnt = lRedContractCnt + clR.Fields("cnt")
        End If
    End Sub

    Private Sub GetAllCancel()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from veritas.dbo.contract c
        inner join veritas.dbo.ContractCancel cc on cc.contractid = c.ContractID
        where status = 'cancelled'
        and not datepaid is null "
        SQL = SQL + "and datepaid < '" & sEndDate2 & "' "
        SQL = SQL + "and canceldate >= '" & sStartDate2 & "' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lAllContractCnt = lAllContractCnt + clR.Fields("cnt")
        End If
    End Sub


End Module
