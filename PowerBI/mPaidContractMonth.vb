﻿Module mPaidContractMonth
    Private sMonthDate As String
    Private sEndMonthDate As String
    Private lCnt As Long
    Private dAmt As Double

    Public Sub PaidContractMonth()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "truncate table veritaspowerbi.dbo.PaidContractMonth "
        clR.RunSQL(SQL, sCON)

        sMonthDate = Month(Today) & "/1/" & Year(Today)
        sMonthDate = DateAdd(DateInterval.Month, -6, CDate(sMonthDate))

        For cnt = 0 To 6
            sEndMonthDate = DateAdd(DateInterval.Month, 1, CDate(sMonthDate))
            CalcCount()
            CalcAmt()
            SQL = "insert into veritaspowerbi.dbo.paidcontractmonth "
            SQL = SQL + "(monthdate, cnt, amt) "
            SQL = SQL + "values ("
            SQL = SQL + "'" & sMonthDate & "', "
            SQL = SQL & lCnt & ", " & dAmt & ")"
            clR.RunSQL(SQL, sCON)
            sMonthDate = DateAdd(DateInterval.Month, 1, CDate(sMonthDate))
        Next

    End Sub

    Private Sub CalcAmt()
        Dim SQL As String
        Dim clR As New clsDBO
        dAmt = 0
        SQL = "select sum(moxydealercost) as amt from contract "
        SQL = SQL + "where datepaid >= '" & sMonthDate & "' "
        SQL = SQL + "and datepaid < '" & sEndMonthDate & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            dAmt = clR.Fields("amt")
        End If
    End Sub

    Private Sub CalcCount()
        Dim SQL As String
        Dim clR As New clsDBO
        lCnt = 0
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where datepaid >= '" & sMonthDate & "' "
        SQL = SQL + "and datepaid < '" & sEndMonthDate & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
        End If
    End Sub


End Module
