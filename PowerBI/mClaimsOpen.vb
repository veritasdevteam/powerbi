﻿Module mClaimsOpen
    Private clCO As New clsDBO
    Private lIDX As Long
    Private lStartIDX As Long
    Private lEndIDX As Long
    Private dDay3Total As Double
    Private dDay5Total As Double
    Private dDay7Total As Double

    Public Sub ClaimsOpenJob()
        ClaimsOpen()
        ClaimsOpenLess()
        ClaimsOpenAN()
        ClaimsOpenANLess()
    End Sub

    Public Sub ClaimsOpenANLess()
        SQL = "select cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate as Reportdate, 
            cl.Moddate as lastmaint, ca.ActivityDesc, vcdsta.totalamt as AmtDue, a.agentname, sa.subagentname,
			case when vcd.cnt is null then 0 else vcd.cnt end as ClaimAge, 
            case when vna.cnt is null then 0 else vna.cnt end as noactivity,
			sc.ServiceCenterName, cl.OpenDate, 
			case when DC.cnt is null then 0 else dc.cnt end as opendays,
			max(ci.RequestDate) as InspectionRequest,
			max(um.MessageDate) as InspectionComplete,
            max(case when ci.RequestDate is null then 0 
			else case when um.MessageDate is null then DATEDIFF(day, ci.requestdate, getdate()) else datediff(day, ci.requestdate, um.messagedate) end 
			end) as InspectionDays
			from claim cl
            inner join contract c on c.contractid = cl.ContractID
            inner join dealer d on c.DealerID = d.DealerID
            left join agents a on d.agentsid = a.agentid
            left join SubAgents sa on sa.SubAgentID = d.SubAgentID
			left join vwClaimAge vcd on cl.ClaimID = vcd.claimid
			left join vwNoActivity vna on cl.claimid = vna.claimid
			left join vwclaimopen vco on cl.claimid = vco.claimid 
            left join ClaimActivity ca on ca.ClaimActivityID = cl.ClaimActivityID
            left join vwClaimDetailSumTotalAmt as VCDSTA on vcdsta.claimid = cl.claimid
            left join ServiceCenter sc on cl.ServiceCenterID = sc.ServiceCenterID
			left join ClaimInspection ci on ci.ClaimID = cl.ClaimID and not InspectionID is null
			left join UserMessage um on ci.InspectionID = um.InspectionID
            left join userinfo ui on ui.userid = cl.assignedto
			left join (select claimid, sum(case when CloseDate is null then DATEDIFF(day, OpenDate, getdate()) else DATEDIFF(day, OpenDate, CloseDate) end) as cnt from ClaimOpenHistory
            group by claimid) DC on cl.claimid = dc.claimid
            where ((cl.CloseDate is null
            and not cl.ClaimActivityID in (8,11,14,15,17,18,21,22,23,24,25,26,28,29,30,31,32,33,35,36,37,38,39,40,42,43,45,46,47,48)
            and cl.Status = 'Open')
            and cl.claimid in (select claimid from vwClaimDetailRequested))
			and d.DealerNo like '2%'
            group by cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate, 
            cl.Moddate, ca.ActivityDesc, vcdsta.totalamt, a.agentname, sa.subagentname,
			case when DC.cnt is null then 0 else dc.cnt end,
            case when vna.cnt is null then 0 else vna.cnt end,
			sc.ServiceCenterName, cl.OpenDate, 
			case when vco.cnt is null then 0 else vco.cnt end,
			case when vcd.cnt is null then 0 else vcd.cnt end
			order by ClaimAge desc "
        clCO.OpenDB(SQL, sCON)
        If clCO.RowCount > 0 Then
            AddClaimsOpenANLess()
            For cnt = 0 To clCO.RowCount - 1
                clCO.GetRowNo(cnt)
                If clCO.Fields("claimage") > 2 Then
                    AddClaimsOpenANLessDay3()
                End If
                If clCO.Fields("claimage") > 4 Then
                    AddClaimsOpenANLessDay5()
                End If
                If clCO.Fields("claimage") > 6 Then
                    AddClaimsOpenANLessDay7()
                End If
            Next
        End If
        SQL = "select * from veritaspowerbi.dbo.claimsopenanLessCallBack "
        SQL = SQL + "where opendate = '" & Today & "' "
        clCO.OpenDB(SQL, sCON)
        If clCO.RowCount > 0 Then
            For cnt = 0 To clCO.RowCount - 1
                clCO.GetRowNo(cnt)
                CalcDayPerANLess()
                CalcDayAvgANLess()
            Next
        End If

    End Sub

    Private Sub CalcDayPerANLess()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenanLessCallBack "
        SQL = SQL + "where idx = " & clCO.Fields("idx")
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("day3per") = CDbl(clR.Fields("day3")) / CDbl(clR.Fields("dayall"))
            If clR.Fields("day5").Length = 0 Then
                clR.Fields("day5") = 0
            End If
            clR.Fields("day5per") = CDbl(clR.Fields("day5")) / CDbl(clR.Fields("dayall"))
            If clR.Fields("day7").Length = 0 Then
                clR.Fields("day7") = 0
            End If
            clR.Fields("day7per") = CDbl(clR.Fields("day7")) / CDbl(clR.Fields("dayall"))
            clR.SaveDB()
        End If
    End Sub
    Private Sub CalcDayAvgANLess()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenanLessCallBack "
        SQL = SQL + "where idx = " & clCO.Fields("idx")
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lEndIDX = clR.Fields("idx")
            lStartIDX = GetStartANLess()
            GetTotalANLess()
            clR.Fields("avg3per") = dDay3Total / 7
            clR.Fields("avg5per") = dDay5Total / 7
            clR.Fields("avg7per") = dDay7Total / 7
        End If
    End Sub

    Private Sub GetTotalANLess()
        Dim clR As New clsDBO
        dDay3Total = 0
        dDay5Total = 0
        dDay7Total = 0
        SQL = "select * from veritaspowerbi.dbo.claimsopenanLessCallBack "
        SQL = SQL + "where idx >= " & lStartIDX & " "
        SQL = SQL + "and idx <= " & lEndIDX
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                dDay3Total = dDay3Total + CDbl(clR.Fields("day3per"))
                dDay5Total = dDay5Total + CDbl(clR.Fields("day5per"))
                dDay7Total = dDay7Total + CDbl(clR.Fields("day7per"))
            Next
        End If
    End Sub

    Private Function GetStartANLess() As Long
        Dim clR As New clsDBO
        GetStartANLess = 0
        SQL = "select top 7 * from veritaspowerbi.dbo.claimsopenanLessCallBack "
        SQL = SQL + "where idx <= " & clCO.Fields("idx")
        SQL = SQL + "order by idx desc "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                GetStartANLess = clR.Fields("idx")
            Next
        End If
    End Function

    Private Sub ClaimsOpenAN()
        SQL = "select cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate as Reportdate, 
            cl.Moddate as lastmaint, ca.ActivityDesc, vcdsta.totalamt as AmtDue, a.agentname, sa.subagentname,
			case when vcd.cnt is null then 0 else vcd.cnt end as ClaimAge, 
            case when vna.cnt is null then 0 else vna.cnt end as noactivity,
			sc.ServiceCenterName, cl.OpenDate, 
			case when DC.cnt is null then 0 else dc.cnt end as opendays,
			max(ci.RequestDate) as InspectionRequest,
			max(um.MessageDate) as InspectionComplete,
            max(case when ci.RequestDate is null then 0 
			else case when um.MessageDate is null then DATEDIFF(day, ci.requestdate, getdate()) else datediff(day, ci.requestdate, um.messagedate) end 
			end) as InspectionDays
			from claim cl
            inner join contract c on c.contractid = cl.ContractID
            inner join dealer d on c.DealerID = d.DealerID
            left join agents a on d.agentsid = a.agentid
            left join SubAgents sa on sa.SubAgentID = d.SubAgentID
			left join vwClaimAge vcd on cl.ClaimID = vcd.claimid
			left join vwNoActivity vna on cl.claimid = vna.claimid
			left join vwclaimopen vco on cl.claimid = vco.claimid 
            left join ClaimActivity ca on ca.ClaimActivityID = cl.ClaimActivityID
            left join vwClaimDetailSumTotalAmt as VCDSTA on vcdsta.claimid = cl.claimid
            left join ServiceCenter sc on cl.ServiceCenterID = sc.ServiceCenterID
			left join ClaimInspection ci on ci.ClaimID = cl.ClaimID and not InspectionID is null
			left join UserMessage um on ci.InspectionID = um.InspectionID
            left join userinfo ui on ui.userid = cl.assignedto
			left join (select claimid, sum(case when CloseDate is null then DATEDIFF(day, OpenDate, getdate()) else DATEDIFF(day, OpenDate, CloseDate) end) as cnt from ClaimOpenHistory
            group by claimid) DC on cl.claimid = dc.claimid
            where ((cl.CloseDate is null
            and not cl.ClaimActivityID in (11,15,17,18,21,22,23,24,25,26,28,29,30,31,32,33,35,36,37,38,39,40,42,43,45,46,47,48)
            and cl.Status = 'Open')
            and cl.claimid in (select claimid from vwClaimDetailRequested))
			and d.DealerNo like '2%'
            group by cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate, 
            cl.Moddate, ca.ActivityDesc, vcdsta.totalamt, a.agentname, sa.subagentname,
			case when DC.cnt is null then 0 else dc.cnt end,
            case when vna.cnt is null then 0 else vna.cnt end,
			sc.ServiceCenterName, cl.OpenDate, 
			case when vco.cnt is null then 0 else vco.cnt end,
			case when vcd.cnt is null then 0 else vcd.cnt end
			order by ClaimAge desc"
        clCO.OpenDB(SQL, sCON)
        If clCO.RowCount > 0 Then
            AddClaimsOpenAN()
            For cnt = 0 To clCO.RowCount - 1
                clCO.GetRowNo(cnt)
                If clCO.Fields("claimage") > 2 Then
                    AddClaimsOpenANDay3()
                End If
                If clCO.Fields("claimage") > 4 Then
                    AddClaimsOpenANDay5()
                End If
                If clCO.Fields("claimage") > 6 Then
                    AddClaimsOpenANDay7()
                End If
            Next
        End If

        SQL = "select * from veritaspowerbi.dbo.claimsopenan "
        SQL = SQL + "where opendate = '" & Today & "' "
        clCO.OpenDB(SQL, sCON)
        If clCO.RowCount > 0 Then
            For cnt = 0 To clCO.RowCount - 1
                clCO.GetRowNo(cnt)
                CalcDayPerAN()
                CalcDayAvgAN()
            Next
        End If

    End Sub

    Private Sub CalcDayPerAN()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenan "
        SQL = SQL + "where idx = " & clCO.Fields("idx")
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("day3per") = CDbl(clR.Fields("day3")) / CDbl(clR.Fields("dayall"))
            clR.Fields("day5per") = CDbl(clR.Fields("day5")) / CDbl(clR.Fields("dayall"))
            If clR.Fields("day7").Length > 0 Then
                clR.Fields("day7per") = CDbl(clR.Fields("day7")) / CDbl(clR.Fields("dayall"))
            Else
                clR.Fields("day7per") = 0
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Function GetStartAN() As Long
        Dim clR As New clsDBO
        GetStartAN = 0
        SQL = "select top 7 * from veritaspowerbi.dbo.claimsopenAN "
        SQL = SQL + "where idx <= " & clCO.Fields("idx")
        SQL = SQL + "order by idx desc "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                GetStartAN = clR.Fields("idx")
            Next
        End If
    End Function

    Private Sub GetTotalAN()
        Dim clR As New clsDBO
        dDay3Total = 0
        dDay5Total = 0
        dDay7Total = 0
        SQL = "select * from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where idx >= " & lStartIDX & " "
        SQL = SQL + "and idx <= " & lEndIDX
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                dDay3Total = dDay3Total + CDbl(clR.Fields("day3per"))
                dDay5Total = dDay5Total + CDbl(clR.Fields("day5per"))
                dDay7Total = dDay7Total + CDbl(clR.Fields("day7per"))
            Next
        End If
    End Sub

    Private Sub CalcDayAvgAN()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenan "
        SQL = SQL + "where idx = " & clCO.Fields("idx")
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lEndIDX = clR.Fields("idx")
            lStartIDX = GetStartAN()
            GetTotalAN()
            clCO.Fields("avg3per") = dDay3Total / 7
            clR.Fields("avg5per") = dDay5Total / 7
            clR.Fields("avg7per") = dDay7Total / 7
        End If
    End Sub

    Private Sub ClaimsOpenLess()
        SQL = "select cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate as Reportdate, 
            cl.Moddate as lastmaint, ca.ActivityDesc, vcdsta.totalamt as AmtDue, a.agentname, sa.subagentname,
			case when vcd.cnt is null then 0 else vcd.cnt end as ClaimAge, 
            case when vna.cnt is null then 0 else vna.cnt end as noactivity,
			sc.ServiceCenterName, cl.OpenDate, 
			case when DC.cnt is null then 0 else dc.cnt end as opendays,
			max(ci.RequestDate) as InspectionRequest,
			max(um.MessageDate) as InspectionComplete,
            max(case when ci.RequestDate is null then 0 
			else case when um.MessageDate is null then DATEDIFF(day, ci.requestdate, getdate()) else datediff(day, ci.requestdate, um.messagedate) end 
			end) as InspectionDays
			from claim cl
            inner join contract c on c.contractid = cl.ContractID
            inner join dealer d on c.DealerID = d.DealerID
            left join agents a on d.agentsid = a.agentid
            left join SubAgents sa on sa.SubAgentID = d.SubAgentID
			left join vwClaimAge vcd on cl.ClaimID = vcd.claimid
			left join vwNoActivity vna on cl.claimid = vna.claimid
			left join vwclaimopen vco on cl.claimid = vco.claimid 
            left join ClaimActivity ca on ca.ClaimActivityID = cl.ClaimActivityID
            left join vwClaimDetailSumTotalAmt as VCDSTA on vcdsta.claimid = cl.claimid
            left join ServiceCenter sc on cl.ServiceCenterID = sc.ServiceCenterID
			left join ClaimInspection ci on ci.ClaimID = cl.ClaimID and not InspectionID is null
			left join UserMessage um on ci.InspectionID = um.InspectionID
            left join userinfo ui on ui.userid = cl.assignedto
			left join (select claimid, sum(case when CloseDate is null then DATEDIFF(day, OpenDate, getdate()) else DATEDIFF(day, OpenDate, CloseDate) end) as cnt from ClaimOpenHistory
            group by claimid) DC on cl.claimid = dc.claimid
            where ((cl.CloseDate is null
            and not cl.ClaimActivityID in (8,11,14,15,17,18,21,22,23,24,25,26,28,29,30,31,32,33,35,36,37,38,39,40,42,43,45,46,47,48)
            and cl.Status = 'Open')
            and cl.claimid in (select claimid from vwClaimDetailRequested))
            group by cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate, 
            cl.Moddate, ca.ActivityDesc, vcdsta.totalamt, a.agentname, sa.subagentname,
			case when DC.cnt is null then 0 else dc.cnt end,
            case when vna.cnt is null then 0 else vna.cnt end,
			sc.ServiceCenterName, cl.OpenDate, 
			case when vco.cnt is null then 0 else vco.cnt end,
			case when vcd.cnt is null then 0 else vcd.cnt end
			order by ClaimAge desc "
        clCO.OpenDB(SQL, sCON)
        If clCO.RowCount > 0 Then
            AddClaimsOpenLess()
            For cnt = 0 To clCO.RowCount - 1
                clCO.GetRowNo(cnt)
                If clCO.Fields("claimage") > 2 Then
                    AddClaimsOpenLessDay3()
                End If
                If clCO.Fields("claimage") > 4 Then
                    AddClaimsOpenLessDay5()
                End If
                If clCO.Fields("claimage") > 6 Then
                    AddClaimsOpenLessDay7()
                End If
            Next
        End If
        SQL = "select * from veritaspowerbi.dbo.claimsopenLessCallBack "
        SQL = SQL + "where day3per is null "
        SQL = SQL + "order by idx "
        clCO.OpenDB(SQL, sCON)
        If clCO.RowCount > 0 Then
            For cnt = 0 To clCO.RowCount - 1
                clCO.GetRowNo(cnt)
                CalcDayPerLess()
                CalcDayAvgLess()
            Next
        End If
    End Sub

    Private Sub CalcDayPerLess()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenLessCallBack "
        SQL = SQL + "where idx = " & clCO.Fields("idx")
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("day3per") = CDbl(clR.Fields("day3")) / CDbl(clR.Fields("dayall"))
            clR.Fields("day5per") = CDbl(clR.Fields("day5")) / CDbl(clR.Fields("dayall"))
            If clR.Fields("day7").Length = 0 Then
                clR.Fields("day7") = 0
            End If
            clR.Fields("day7per") = CDbl(clR.Fields("day7")) / CDbl(clR.Fields("dayall"))
            clR.SaveDB()
        End If
    End Sub

    Private Sub CalcDayAvgLess()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenLessCallBack "
        SQL = SQL + "where idx = " & clCO.Fields("idx")
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lEndIDX = clR.Fields("idx")
            lStartIDX = GetStartless()
            GetTotalLess()
            clR.Fields("avg3per") = dDay3Total / 7
            clR.Fields("avg5per") = dDay5Total / 7
            clR.Fields("avg7per") = dDay7Total / 7
        End If
    End Sub

    Private Function GetStartless() As Long
        Dim clR As New clsDBO
        GetStartless = 0
        SQL = "select top 7 * from veritaspowerbi.dbo.claimsopenLessCallBack "
        SQL = SQL + "where idx <= " & clCO.Fields("idx")
        SQL = SQL + "order by idx desc "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                GetStartless = clR.Fields("idx")
            Next
        End If
    End Function

    Private Sub GetTotalLess()
        Dim clR As New clsDBO
        dDay3Total = 0
        dDay5Total = 0
        dDay7Total = 0
        SQL = "select * from veritaspowerbi.dbo.claimsopenLessCallBack "
        SQL = SQL + "where idx >= " & lStartIDX & " "
        SQL = SQL + "and idx <= " & lEndIDX
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                dDay3Total = dDay3Total + CDbl(clR.Fields("day3per"))
                dDay5Total = dDay5Total + CDbl(clR.Fields("day5per"))
                dDay7Total = dDay7Total + CDbl(clR.Fields("day7per"))
            Next
        End If
    End Sub

    Private Sub ClaimsOpen()
        SQL = "select cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate as Reportdate, 
            cl.Moddate as lastmaint, ca.ActivityDesc, vcdsta.totalamt as AmtDue, a.agentname, sa.subagentname,
			case when vcd.cnt is null then 0 else vcd.cnt end as ClaimAge, 
            case when vna.cnt is null then 0 else vna.cnt end as noactivity,
			sc.ServiceCenterName, cl.OpenDate, 
			case when DC.cnt is null then 0 else dc.cnt end as opendays,
			max(ci.RequestDate) as InspectionRequest,
			max(um.MessageDate) as InspectionComplete,
            max(case when ci.RequestDate is null then 0 
			else case when um.MessageDate is null then DATEDIFF(day, ci.requestdate, getdate()) else datediff(day, ci.requestdate, um.messagedate) end 
			end) as InspectionDays
			from claim cl
            inner join contract c on c.contractid = cl.ContractID
            inner join dealer d on c.DealerID = d.DealerID
            left join agents a on d.agentsid = a.agentid
            left join SubAgents sa on sa.SubAgentID = d.SubAgentID
			left join vwClaimAge vcd on cl.ClaimID = vcd.claimid
			left join vwNoActivity vna on cl.claimid = vna.claimid
			left join vwclaimopen vco on cl.claimid = vco.claimid 
            left join ClaimActivity ca on ca.ClaimActivityID = cl.ClaimActivityID
            left join vwClaimDetailSumTotalAmt as VCDSTA on vcdsta.claimid = cl.claimid
            left join ServiceCenter sc on cl.ServiceCenterID = sc.ServiceCenterID
			left join ClaimInspection ci on ci.ClaimID = cl.ClaimID and not InspectionID is null
			left join UserMessage um on ci.InspectionID = um.InspectionID
            left join userinfo ui on ui.userid = cl.assignedto
			left join (select claimid, sum(case when CloseDate is null then DATEDIFF(day, OpenDate, getdate()) else DATEDIFF(day, OpenDate, CloseDate) end) as cnt from ClaimOpenHistory
            group by claimid) DC on cl.claimid = dc.claimid
            where
            cl.Status = 'Open'
            and cl.claimid in (select claimid from vwClaimDetailRequested)
            group by cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate, 
            cl.Moddate, ca.ActivityDesc, vcdsta.totalamt, a.agentname, sa.subagentname,
			case when DC.cnt is null then 0 else dc.cnt end,
            case when vna.cnt is null then 0 else vna.cnt end,
			sc.ServiceCenterName, cl.OpenDate, 
			case when vco.cnt is null then 0 else vco.cnt end,
			case when vcd.cnt is null then 0 else vcd.cnt end
			order by ClaimAge desc"
        clCO.OpenDB(SQL, sCON)
        If clCO.RowCount > 0 Then
            AddClaimsOpen()
            For cnt = 0 To clCO.RowCount - 1
                clCO.GetRowNo(cnt)
                If clCO.Fields("claimage") > 2 Then
                    AddClaimsOpenDay3()
                End If
                If clCO.Fields("claimage") > 4 Then
                    AddClaimsOpenDay5()
                End If
                If clCO.Fields("claimage") > 6 Then
                    AddClaimsOpenDay7()
                End If
            Next
        End If
        SQL = "select * from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where opendate = '" & Today & "' "
        clCO.OpenDB(SQL, sCON)
        If clCO.RowCount > 0 Then
            For cnt = 0 To clCO.RowCount - 1
                clCO.GetRowNo(cnt)
                CalcDayPer()
                CalcDayAvg()
            Next
        End If
    End Sub

    Private Sub CalcDayPer()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where idx = " & clCO.Fields("idx")
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("day3per") = CDbl(clR.Fields("day3")) / CDbl(clR.Fields("dayall"))
            clR.Fields("day5per") = CDbl(clR.Fields("day5")) / CDbl(clR.Fields("dayall"))
            clR.Fields("day7per") = CDbl(clR.Fields("day7")) / CDbl(clR.Fields("dayall"))
            clR.SaveDB()
        End If
    End Sub

    Private Function GetStart() As Long
        Dim clR As New clsDBO
        GetStart = 0
        SQL = "select top 7 * from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where idx <= " & clCO.Fields("idx")
        SQL = SQL + "order by idx desc "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                GetStart = clR.Fields("idx")
            Next
        End If
    End Function

    Private Sub GetTotal()
        Dim clR As New clsDBO
        dDay3Total = 0
        dDay5Total = 0
        dDay7Total = 0
        SQL = "select * from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where idx >= " & lStartIDX & " "
        SQL = SQL + "and idx <= " & lEndIDX
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                dDay3Total = dDay3Total + CDbl(clR.Fields("day3per"))
                dDay5Total = dDay5Total + CDbl(clR.Fields("day5per"))
                dDay7Total = dDay7Total + CDbl(clR.Fields("day7per"))
            Next
        End If
    End Sub

    Private Sub CalcDayAvg()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where idx = " & clCO.Fields("idx")
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lEndIDX = clR.Fields("idx")
            lStartIDX = GetStart()
            GetTotal()
            clCO.Fields("avg3per") = dDay3Total / 7
            clR.Fields("avg5per") = dDay5Total / 7
            clR.Fields("avg7per") = dDay7Total / 7
        End If
    End Sub

    Private Sub AddClaimsOpenANLessDay7()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenANLessCallBack "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("day7").Length = 0 Then
                clR.Fields("day7") = 1
            Else
                clR.Fields("day7") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenANDay7()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenAN "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("day7").Length = 0 Then
                clR.Fields("day7") = 1
            Else
                clR.Fields("day7") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenLessDay7()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenLessCallBack "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("day7").Length = 0 Then
                clR.Fields("day7") = 1
            Else
                clR.Fields("day7") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenDay7()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("day7").Length = 0 Then
                clR.Fields("day7") = 1
            Else
                clR.Fields("day7") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenANLessDay5()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenANLessCallBack "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("day5").Length = 0 Then
                clR.Fields("day5") = 1
            Else
                clR.Fields("day5") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenANDay5()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenAN "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("day5").Length = 0 Then
                clR.Fields("day5") = 1
            Else
                clR.Fields("day5") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenLessDay5()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenLessCallBack "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("day5").Length = 0 Then
                clR.Fields("day5") = 1
            Else
                clR.Fields("day5") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenDay5()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("day5").Length = 0 Then
                clR.Fields("day5") = 1
            Else
                clR.Fields("day5") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenANLessDay3()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenANLessCallBack "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("day3").Length = 0 Then
                clR.Fields("day3") = 1
            Else
                clR.Fields("day3") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenANDay3()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenAN "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("day3").Length = 0 Then
                clR.Fields("day3") = 1
            Else
                clR.Fields("day3") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenLessDay3()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenLessCallBack "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("day3").Length = 0 Then
                clR.Fields("day3") = 1
            Else
                clR.Fields("day3") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenDay3()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lIDX = clR.Fields("idx")
            If clR.Fields("day3").Length = 0 Then
                clR.Fields("day3") = 1
            Else
                clR.Fields("day3") = CLng(clR.Fields("day3")) + 1
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenANLess()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenANLessCallBack "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("opendate") = Today
            clR.Fields("dayall") = clCO.RowCount
            clR.AddRow()
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenAN()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenAN "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("opendate") = Today
            clR.Fields("dayall") = clCO.RowCount
            clR.AddRow()
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpenLess()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopenLessCallBack "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("opendate") = Today
            clR.Fields("dayall") = clCO.RowCount
            clR.AddRow()
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddClaimsOpen()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.claimsopen "
        SQL = SQL + "where opendate = '" & Today & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("opendate") = Today
            clR.Fields("dayall") = clCO.RowCount
            clR.AddRow()
            clR.SaveDB()
        End If
    End Sub
End Module
