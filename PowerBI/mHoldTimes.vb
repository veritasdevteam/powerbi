﻿Module mHoldTimes
    Dim sStartWeek1 As String
    Dim sEndWeek1 As String
    Dim sStartWeek2 As String
    Dim sEndWeek2 As String
    Private clCG As New clsDBO
    Private sCallGroup As String
    Private lInboundCalls As Long
    Private dAvgQueDur As Double
    Private dAvgQueDurPrev As Double
    Private dAvgQueDurYear As Double
    Private dVariance As Double
    Private dVariancePrev As Double
    Private dChange As Double
    Private dMaxQueue As Double
    Private dMaxQueuePrev As Double
    Private dMaxVariance As Double

    Public Sub HoldTimes()
        sEndWeek1 = DateAdd(DateInterval.Day, -1, Today)
        sStartWeek1 = DateAdd(DateInterval.Day, -6, CDate(sEndWeek1))
        sEndWeek2 = DateAdd(DateInterval.Day, -1, CDate(sStartWeek1))
        sStartWeek2 = DateAdd(DateInterval.Day, -6, CDate(sEndWeek2))
        HoldTimeDate()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.holdtime "
        clR.RunSQL(SQL, sCON)
        ProcessHoldTime()
    End Sub

    Private Sub ProcessHoldTime()
        SQL = "select CallGroup from veritasphone.dbo.GroupAvgQueueHold "
        SQL = SQL + "where calldate >= '" & sStartWeek1 & "' "
        SQL = SQL + "and calldate <= '" & sEndWeek1 & "' "
        SQL = SQL + "group by callgroup "
        SQL = SQL + "having sum(inboundcalls) > 0 "
        clCG.OpenDB(SQL, sCON)
        If clCG.RowCount > 0 Then
            For cnt = 0 To clCG.RowCount - 1
                clCG.GetRowNo(cnt)
                sCallGroup = clCG.Fields("callgroup")
                CalcInboundCalls()
                CalcAvgQueDur()
                CalcAvgQueDurPrev()
                CalcAvgQueDurYear()
                dVariance = dAvgQueDur - dAvgQueDurYear
                dVariancePrev = dAvgQueDurPrev - dAvgQueDurYear
                dChange = dVariance - dVariancePrev
                CalcMaxQue()
                CalcMaxQuePrev()
                AddRecords()
            Next
        End If
    End Sub

    Private Sub AddRecords()
        Dim clR As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.HoldTime "
        clR.OpenDB(SQL, sCON)
        clR.NewRow()
        clR.Fields("type") = sCallGroup.Trim
        clR.Fields("inboundcalls") = lInboundCalls
        clR.Fields("AvgQueueDuration") = Format(dAvgQueDur, "###.0")
        clR.Fields("avgqueue") = Format(dAvgQueDurYear, "###.00")
        clR.Fields("variance") = Format(dVariance, "###.0")
        clR.Fields("change") = Format(dChange, "###.0")
        clR.Fields("maxqueueduration") = Format(dMaxQueue, "###.0")
        clR.Fields("maxqueuelastweek") = Format(dMaxQueuePrev, "###.0")
        clR.Fields("variancelastweek") = Format(dMaxQueue - dMaxQueuePrev, "###.0")
        clR.AddRow()
        clR.SaveDB()
    End Sub

    Private Sub CalcMaxQuePrev()
        Dim clR As New clsDBO
        SQL = "select max(maxqueuedur) as ic from veritasphone.dbo.GroupAvgQueueHold "
        SQL = SQL + "where calldate >= '" & sStartWeek2 & "' "
        SQL = SQL + "and calldate <= '" & sEndWeek2 & "' "
        SQL = SQL + "and callgroup = '" & sCallGroup & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            dMaxQueuePrev = clR.Fields("ic")
        End If
        dMaxQueuePrev = dMaxQueuePrev / 60
    End Sub

    Private Sub CalcMaxQue()
        Dim clR As New clsDBO
        SQL = "select max(maxqueuedur) as ic from veritasphone.dbo.GroupAvgQueueHold "
        SQL = SQL + "where calldate >= '" & sStartWeek1 & "' "
        SQL = SQL + "and calldate <= '" & sEndWeek1 & "' "
        SQL = SQL + "and callgroup = '" & sCallGroup & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            dMaxQueue = clR.Fields("ic")
        End If
        dMaxQueue = dMaxQueue / 60
    End Sub

    Private Sub CalcAvgQueDurYear()
        Dim clR As New clsDBO
        SQL = "select sum(avgqueuedur) as ic, count(*) as cnt from veritasphone.dbo.GroupAvgQueueHold "
        SQL = SQL + "where calldate >= '" & "1/1/2021" & "' "
        SQL = SQL + "and calldate <= '" & "12/31/2021" & "' "
        SQL = SQL + "and callgroup = '" & sCallGroup & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            dAvgQueDurYear = clR.Fields("ic")
            dAvgQueDurYear = dAvgQueDurYear / clR.Fields("cnt")
        End If
        dAvgQueDurYear = dAvgQueDurYear / 60
    End Sub

    Private Sub CalcAvgQueDurPrev()
        Dim clR As New clsDBO
        SQL = "select sum(avgqueuedur) as ic, count(*) as cnt from veritasphone.dbo.GroupAvgQueueHold "
        SQL = SQL + "where calldate >= '" & sStartWeek2 & "' "
        SQL = SQL + "and calldate <= '" & sEndWeek2 & "' "
        SQL = SQL + "and callgroup = '" & sCallGroup & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            dAvgQueDurPrev = clR.Fields("ic")
            dAvgQueDurPrev = dAvgQueDurPrev / clR.Fields("cnt")
        End If
        dAvgQueDurPrev = dAvgQueDurPrev / 60
    End Sub

    Private Sub CalcAvgQueDur()
        Dim clR As New clsDBO
        SQL = "select sum(avgqueuedur) as ic, count(*) as cnt from veritasphone.dbo.GroupAvgQueueHold "
        SQL = SQL + "where calldate >= '" & sStartWeek1 & "' "
        SQL = SQL + "and calldate <= '" & sEndWeek1 & "' "
        SQL = SQL + "and callgroup = '" & sCallGroup & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            dAvgQueDur = clR.Fields("ic")
            dAvgQueDur = dAvgQueDur / clR.Fields("cnt")
        End If
        dAvgQueDur = dAvgQueDur / 60
    End Sub

    Private Sub CalcInboundCalls()
        Dim clR As New clsDBO
        SQL = "select sum(inboundcalls) as ic from veritasphone.dbo.GroupAvgQueueHold "
        SQL = SQL + "where calldate >= '" & sStartWeek1 & "' "
        SQL = SQL + "and calldate <= '" & sEndWeek1 & "' "
        SQL = SQL + "and callgroup = '" & sCallGroup & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lInboundCalls = clR.Fields("ic")
        End If
    End Sub

    Private Sub HoldTimeDate()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.holdtimedate "
        clR.OpenDB(SQL, sCON)
        SQL = "select * from veritaspowerbi.dbo.holdtimedate "
        clR.OpenDB(SQL, sCON)
        clR.NewRow()
        clR.Fields("weekdate") = Format(CDate(sStartWeek1), "M/d/yyyy") & " - " & Format(CDate(sEndWeek1), "M/d/yyyy")
        clR.AddRow()
        clR.SaveDB()
    End Sub

End Module
