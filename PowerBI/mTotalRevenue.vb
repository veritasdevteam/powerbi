﻿Module mTotalRevenue
    Private sStartDate As String
    Private sEndDate As String
    Private dAmt As Double

    Public Sub TotalRevenue()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.totalrevenue "
        clR.RunSQL(SQL, sCON)
        sStartDate = DateAdd(DateInterval.Month, -12, Today)
        sStartDate = Month(CDate(sStartDate)) & "/1/" & Year(CDate(sStartDate))
        sEndDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
        Do Until CDate(sStartDate) > Today
            calcrevenue
            addrevenue

            sStartDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
            sEndDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
        Loop
    End Sub

    Private Sub AddRevenue()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.totalrevenue "
        SQL = SQL + "(MonthDate, amt) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartDate & "', "
        SQL = SQL & dAmt & ") "
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcRevenue()
        Dim clR As New clsDBO
        SQL = "select sum(moxydealercost) as Amt from contract "
        SQL = SQL + "where datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and datepaid < '" & sEndDate & "' "
        SQL = SQL + "and not ProgramID in (47,48,51,54,55,56,57,58,59,60,101,103,105,67) "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("amt").Length > 0 Then
                dAmt = clR.Fields("amt")
            Else
                dAmt = 0
            End If
        End If
    End Sub
End Module
