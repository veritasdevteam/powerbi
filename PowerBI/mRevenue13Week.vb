﻿Module mRevenue13Week
    Private sStartWeek As String
    Private sEndWeek As String
    Private lCnt As Long
    Private dAmt As Double

    Public Sub Revenue13Week()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.revenue13week "
        clR.RunSQL(SQL, sCON)
        sStartWeek = DateAdd(DateInterval.Month, -3, Today)
        sEndWeek = DateAdd(DateInterval.Day, 6, CDate(sStartWeek))
        Do Until CDate(sStartWeek) > Today
            GetAmts()
            addamts
            sStartWeek = DateAdd(DateInterval.Day, 7, CDate(sStartWeek))
            sEndWeek = DateAdd(DateInterval.Day, 6, CDate(sStartWeek))
        Loop
    End Sub

    Private Sub AddAmts()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.revenue13week "
        SQL = SQL + "(weekdate, cnt, amt) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartWeek & "', "
        SQL = SQL & lCnt & ", "
        SQL = SQL & dAmt & ") "
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub GetAmts()
        Dim sStartProc As String
        Dim sEndProc As String
        Dim clR As New clsDBO
        dAmt = 0
        sStartProc = sStartWeek
        sEndProc = sEndWeek
        SQL = "select count(*) as cnt, sum(moxydealercost) as Amt from contract "
        SQL = SQL + "where datepaid >= '" & sStartProc & "' "
        SQL = SQL + "and datepaid <= '" & sEndProc & "' "
        SQL = SQL + "and not ProgramID in (47,48,51,54,55,56,57,58,59,60,101,103,105,67) "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
            If clR.Fields("amt").Length > 0 Then
                dAmt = clR.Fields("amt")
            End If
            lCnt = lCnt
            dAmt = dAmt
        End If
    End Sub
End Module
