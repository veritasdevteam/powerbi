﻿Module mAgentPaid

    Dim sStartDate As String
    Dim sEndDate As String
    Dim lTotal As Long
    Dim lBerman As Long
    Dim lBermanTotal As Long = 0
    Dim lAN As Long
    Dim lANTotal As Long
    Dim lEP As Long
    Dim lEPTotal As Long
    Dim lTASA As Long
    Dim lTASATotal As Long
    Dim lPrime As Long
    Dim lPrimeTotal As Long
    Dim lOtherTotal As Long

    Public Sub AgentPaid()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.AgentPaidCount "
        clR.RunSQL(SQL, sCON)
        SQL = "truncate table veritaspowerbi.dbo.AgentPaidTotal "
        clR.RunSQL(SQL, sCON)

        sStartDate = "1/1/2020"
        Do Until CDate(sStartDate) > Today
            sEndDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
            CalcTotal()
            CalcBerman()
            CalcAN()
            CalcEP()
            CalcTASA()
            CalcOthers()
            sStartDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
        Loop
        CalcRunRateCount()
        PlaceTotal()
    End Sub

    Private Sub CalcRunRateCount()
        Dim clR As New clsDBO
        Dim clM As New clsDBO
        Dim sMonth As String
        Dim lDay As Long
        Dim lTotalDays As Long
        Dim lTemp As Long
        Dim lTemp2 As Long
        sMonth = Month(Today) & "/1/" & Year(Today)
        lDay = Day(Today)
        lDay = lDay - 1
        lTotalDays = DateDiff(DateInterval.Day, CDate(sMonth), DateAdd(DateInterval.Month, 1, CDate(sMonth)))
        SQL = "select * from veritaspowerbi.dbo.agentPaidCount "
        SQL = SQL + "where monthdate = '" & sMonth & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from veritaspowerbi.dbo.agentpaidcount "
                SQL = SQL + "where idx = " & clR.Fields("idx")
                clM.OpenDB(SQL, sCON)
                If clM.RowCount > 0 Then
                    clM.GetRow()
                    lTemp = clM.Fields("cnt")
                    If lTemp > 0 Then
                        lTemp = (lTemp / lDay) * lTotalDays
                    End If
                    lTemp2 = CLng(clM.Fields("cnt"))
                    lTemp2 = lTemp - lTemp2
                    If clM.Fields("agent") = "C Berman" Then
                        lBermanTotal = lBermanTotal + lTemp2
                    End If
                    If clM.Fields("agent") = "AutoNation" Then
                        lANTotal = lANTotal + lTemp2
                    End If
                    If clM.Fields("agent") = "Express Protect" Then
                        lEPTotal = lEPTotal + lTemp2
                    End If
                    If clM.Fields("agent") = "TASA" Then
                        lTASATotal = lTASATotal + lTemp2
                    End If
                    If clM.Fields("agent") = "Others" Then
                        lOtherTotal = lOtherTotal + lTemp2
                    End If


                    clM.Fields("cnt") = lTemp
                    clM.SaveDB()
                End If
            Next
        End If
    End Sub

    Private Sub PlaceTotal()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.AgentPaidTotal "
        SQL = SQL + "(Agent, Cnt) "
        SQL = SQL + "values ('C Berman', " & lBermanTotal & ")"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.AgentPaidTotal "
        SQL = SQL + "(Agent, Cnt) "
        SQL = SQL + "values ('AN', " & lANTotal & ")"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.AgentPaidTotal "
        SQL = SQL + "(Agent, Cnt) "
        SQL = SQL + "values ('Express Protect', " & lEPTotal & ")"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.AgentPaidTotal "
        SQL = SQL + "(Agent, Cnt) "
        SQL = SQL + "values ('TASA', " & lTASATotal & ")"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.AgentPaidTotal "
        SQL = SQL + "(Agent, Cnt) "
        SQL = SQL + "values ('Others', " & lOtherTotal & ")"
        clR.RunSQL(SQL, sCON)

    End Sub

    Private Sub CalcOthers()
        Dim clP As New clsDBO
        SQL = "select * from veritaspowerbi.dbo.AgentPaidCount "
        SQL = SQL + "where agent = 'Others' "
        SQL = SQL + "and monthdate = '" & sStartDate & "' "
        clP.OpenDB(SQL, sCON)
        If clP.RowCount > 0 Then
            clP.GetRow()
        Else
            clP.NewRow()
        End If
        clP.Fields("agent") = "Others"
        clP.Fields("monthdate") = sStartDate
        clP.Fields("cnt") = lTotal - lBerman - lAN - lEP - lTASA
        lOtherTotal = lOtherTotal + lTotal - lBerman - lAN - lEP - lTASA
        If clP.RowCount = 0 Then
            clP.AddRow()
        End If
        clP.SaveDB()
    End Sub

    Private Sub CalcTASA()
        Dim clR As New clsDBO
        Dim clP As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and datepaid < '" & sEndDate & "' "
        SQL = SQL + "and agentsid = 36"
        SQL = SQL + "and not contractno like 'vep%' "
        SQL = SQL + "and not contractno like 'Rep%' "
        SQL = SQL + "and dealerid in (select dealerid from dealer where prime = 0)"
        SQL = SQL + "and not ProgramID in (47,48,51,54,55,56,57,58,59,60,101,103,105,67) "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lTASA = clR.Fields("cnt")
            lTASATotal = lTASATotal + lTASA
            SQL = "select * from veritaspowerbi.dbo.AgentPaidCount "
            SQL = SQL + "where agent = 'TASA' "
            SQL = SQL + "and monthdate = '" & sStartDate & "' "
            clP.OpenDB(SQL, sCON)
            If clP.RowCount > 0 Then
                clP.GetRow()
            Else
                clP.NewRow()
            End If
            clP.Fields("agent") = "TASA"
            clP.Fields("monthdate") = sStartDate
            clP.Fields("cnt") = lTASA
            If clP.RowCount = 0 Then
                clP.AddRow()
            End If
            clP.SaveDB()
        End If
    End Sub

    Private Sub CalcEP()
        Dim clR As New clsDBO
        Dim clP As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and datepaid < '" & sEndDate & "' "
        SQL = SQL + "and (contractno like 'vep%' "
        SQL = SQL + "or contractno like 'Rep%') "
        SQL = SQL + "and dealerid in (select dealerid from dealer where prime = 0)"
        SQL = SQL + "and not ProgramID in (47,48,51,54,55,56,57,58,59,60,101,103,105,67) "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lEP = clR.Fields("cnt")
            lEPTotal = lEPTotal + lEP
            SQL = "select * from veritaspowerbi.dbo.AgentPaidCount "
            SQL = SQL + "where agent = 'Express Protect' "
            SQL = SQL + "and monthdate = '" & sStartDate & "' "
            clP.OpenDB(SQL, sCON)
            If clP.RowCount > 0 Then
                clP.GetRow()
            Else
                clP.NewRow()
            End If
            clP.Fields("agent") = "Express Protect"
            clP.Fields("monthdate") = sStartDate
            clP.Fields("cnt") = lEP
            If clP.RowCount = 0 Then
                clP.AddRow()
            End If
            clP.SaveDB()
        End If
    End Sub

    Private Sub CalcAN()
        Dim clR As New clsDBO
        Dim clP As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and datepaid < '" & sEndDate & "' "
        SQL = SQL + "and not contractno like 'vep%' "
        SQL = SQL + "and not contractno like 'rep%' "
        SQL = SQL + "and agentsid = 1251 "
        SQL = SQL + "and dealerid in (select dealerid from dealer where prime = 0)"
        SQL = SQL + "and not ProgramID in (47,48,51,54,55,56,57,58,59,60,101,103,105,67) "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lAN = clR.Fields("cnt")
            lANTotal = lANTotal + lAN
            SQL = "select * from veritaspowerbi.dbo.AgentPaidCount "
            SQL = SQL + "where agent = 'AutoNation' "
            SQL = SQL + "and monthdate = '" & sStartDate & "' "
            clP.OpenDB(SQL, sCON)
            If clP.RowCount > 0 Then
                clP.GetRow()
            Else
                clP.NewRow()
            End If
            clP.Fields("agent") = "AutoNation"
            clP.Fields("monthdate") = sStartDate
            clP.Fields("cnt") = lAN
            If clP.RowCount = 0 Then
                clP.AddRow()
            End If
            clP.SaveDB()
        End If
    End Sub

    Private Sub CalcBerman()
        Dim clR As New clsDBO
        Dim clP As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and datepaid < '" & sEndDate & "' "
        SQL = SQL + "and agentsid = 1 "
        SQL = SQL + "and dealerid in (select dealerid from dealer where prime = 0)"
        SQL = SQL + "and not ProgramID in (47,48,51,54,55,56,57,58,59,60,101,103,105,67) "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lBerman = clR.Fields("cnt")
            lBermanTotal = lBermanTotal + lBerman
            SQL = "select * from veritaspowerbi.dbo.AgentPaidCount "
            SQL = SQL + "where agent = 'C Berman' "
            SQL = SQL + "and monthdate = '" & sStartDate & "' "
            clP.OpenDB(SQL, sCON)
            If clP.RowCount > 0 Then
                clP.GetRow()
            Else
                clP.NewRow()
            End If
            clP.Fields("agent") = "C Berman"
            clP.Fields("monthdate") = sStartDate
            clP.Fields("cnt") = lBerman
            If clP.RowCount = 0 Then
                clP.AddRow()
            End If
            clP.SaveDB()
        End If
    End Sub

    Private Sub CalcTotal()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where datepaid >= '" & sStartDate & "' "
        SQL = SQL + "and datepaid < '" & sEndDate & "' "
        SQL = SQL + "and not ProgramID in (47,48,51,54,55,56,57,58,59,60,101,103,105,67) "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lTotal = clR.Fields("cnt")
        End If
    End Sub


End Module
