﻿Module mWexDaily
    Public Sub WexDaily()
        ClearTable()
        Dim clR As New clsDBO
        Dim lDay As Long = 0
        Dim cnt As Double = 0
        Dim dDate As String = ""
        Dim SQL As String = ""
        Dim A1 As Date = Today
        Dim A2 As Date = New Date(2020, 3, 4)
        Dim DiffResult As TimeSpan = CType(A1 - A2, TimeSpan)
        lDay = DiffResult.Days

        Do
            dDate = A2.AddDays(cnt).ToString()
            SQL = "insert into veritaspowerbi.dbo.claimwexdaily " +
                  "(paiddate, dailytype, Amt) " +
                  "select '" + dDate + "', " +
                  "'All', " +
                  "avg(paidamt) " +
                  "from (" +
                  "select  sum(cd.paidamt) as PaidAmt " +
                  "from Claimdetail cd " +
                  "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                  "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                  "where cd.datepaid = '" + dDate + "' " +
                  "and not cp.DateTransmitted is null " +
                  "and not cp.wexcode is null " +
                  "and cd.ClaimDetailStatus = 'Paid' " +
                  "and paidamt <> 0 " +
                  "group by ClaimID) aa"
            clR.RunSQL(SQL, sCON)

            SQL = "insert into veritaspowerbi.dbo.claimwexdaily " +
                  "(paiddate, dailytype, Amt) " +
                  "select '" + dDate + "', " +
                  "'All No Ancillary', " +
                  "avg(paidamt) " +
                  "from (" +
                  "select  sum(cd.paidamt) as PaidAmt " +
                  "from Claimdetail cd " +
                  "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                  "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                  "inner join claim cl on cl.claimid = cd.claimid " +
                  "inner join contract c on c.contractid = cl.contractid " +
                  "where cd.datepaid = '" + dDate + "' " +
                  "and not cp.DateTransmitted is null " +
                  "and cd.ClaimDetailStatus = 'Paid' " +
                  "and paidamt <> 0 " +
                  "and not c.programid in (67,103,105)" +
                  "group by cd.ClaimID) aa"
            clR.RunSQL(SQL, sCON)

            SQL = "insert into veritaspowerbi.dbo.claimwexdaily " +
                  "(paiddate, dailytype, Amt) " +
                  "select '" + dDate + "', " +
                  "'All No AN', " +
                  "case when avg(paidamt) is null then 0 else avg(paidamt) end " +
                  "from (" +
                  "select  sum(cd.paidamt) as PaidAmt " +
                  "from Claimdetail cd " +
                  "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                  "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                  "inner join claim cl on cl.claimid = cd.claimid " +
                  "inner join contract c on c.contractid = cl.contractid " +
                  "where cd.datepaid = '" + dDate + "' " +
                  "and not cp.DateTransmitted is null and cd.ClaimDetailStatus = 'Paid' " +
                  "and paidamt <> 0 " +
                  "and an = 0 " +
                  "group by cd.ClaimID) aa"
            clR.RunSQL(SQL, sCON)

            SQL = "insert into veritaspowerbi.dbo.claimwexdaily " +
                  "(paiddate, dailytype, Amt) " +
                  "select '" + dDate + "', " +
                  "'All no Ancillary/AN', " +
                  "case when avg(paidamt) is null then 0 else avg(paidamt) end " +
                  "from (" +
                  "select  sum(cd.paidamt) as PaidAmt " +
                  "from Claimdetail cd " +
                  "inner join ClaimPaymentLink cpl on cd.ClaimDetailID = cpl.ClaimDetailID " +
                  "inner join ClaimPayment cp on cp.ClaimPaymentID = cpl.ClaimPaymentID " +
                  "inner join claim cl on cl.claimid = cd.claimid " +
                  "inner join contract c on c.contractid = cl.contractid " +
                  "where cd.datepaid = '" + dDate + "' " +
                  "and not cp.DateTransmitted is null and cd.ClaimDetailStatus = 'Paid' " +
                  "and paidamt <> 0 " +
                  "and not c.programid in (67,103,105) " +
                  "and an = 0 " +
                  "group by cd.ClaimID) aa"
            clR.RunSQL(SQL, sCON)

            cnt += 1
        Loop While (cnt < lDay)
    End Sub

    Private Sub ClearTable()
        Dim clR As New clsDBO
        Dim SQL As String = ""
        SQL = "truncate table veritaspowerbi.dbo.claimwexdaily "
        clR.RunSQL(SQL, sCON)
    End Sub
End Module
