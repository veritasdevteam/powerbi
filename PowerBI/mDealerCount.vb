﻿Module mDealerCount
    Private sStartDate As String
    Private sEndDate As String
    Private lAllDealers As Long
    Private lNewDealers As Long
    Private lSignUp As Long


    Public Sub DealerCount()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.dealercounts "
        clR.RunSQL(SQL, sCON)
        sStartDate = "1/3/2021"
        Do Until CDate(sStartDate) > Today
            sEndDate = DateAdd(DateInterval.Day, 7, CDate(sStartDate))
            lAllDealers = 0
            lNewDealers = 0
            lSignUp = 0
            CalcAllDealers()
            CalcNewDealers()
            CalcSignUp()
            AddData()
            sStartDate = DateAdd(DateInterval.Day, 7, CDate(sStartDate))
        Loop
        CalcRunRate()

    End Sub

    Private Sub CalcRunRate()
        Dim clR As New clsDBO
        Dim lDays As Long
        sStartDate = DateAdd(DateInterval.Day, -7, CDate(sStartDate))
        lDays = DateDiff(DateInterval.Day, CDate(sStartDate), Today)
        SQL = "select * from veritaspowerbi.dbo.dealercounts "
        SQL = SQL + "where weekdate = '" & sStartDate & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount Then
            clR.GetRow()
            If lDays > 0 Then
                clR.Fields("alldealers") = CInt((clR.Fields("alldealers") / lDays) * 7)
            End If
            clR.SaveDB()
        End If

    End Sub

    Private Sub CalcSignUp()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from dealer "
        SQL = SQL + "where DateSigned >= '" & DateAdd(DateInterval.Month, -6, CDate(sEndDate)) & "' "
        SQL = SQL + "and DateSigned < '" & sEndDate & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lSignUp = clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub AddData()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.dealercounts "
        SQL = SQL + "(weekdate, alldealers, newdealers, signup) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartDate & "', "
        SQL = SQL & lAllDealers & ","
        SQL = SQL & lNewDealers & ","
        SQL = SQL & lSignUp & ""
        SQL = SQL + ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcNewDealers()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from dealer "
        SQL = SQL + "where DateSigned >= '" & sStartDate & "' "
        SQL = SQL + "and DateSigned < '" & sEndDate & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()

            If clR.Fields("cnt") > 0 Then
                lNewDealers = clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcAllDealers()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from dealer "
        SQL = SQL + "where dealerid in ("
        SQL = SQL + "select dealerid from contract "
        SQL = SQL + "where saledate >= '" & sStartDate & "' "
        SQL = SQL + "and saledate < '" & sEndDate & "' "
        SQL = SQL + "and not status is null "
        SQL = SQL + "and status <> 'Invalid' "
        SQL = SQL + "and status <> 'Quote' "
        SQL = SQL + "and status <> 'Rejected' "
        SQL = SQL + "and status <> 'Sale pending' "
        SQL = SQL + "and status <> 'test' "
        SQL = SQL + "and status <> 'Training' "
        SQL = SQL + "and status <> 'Void') "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()

            If clR.Fields("cnt") > 0 Then
                lAllDealers = clR.Fields("cnt")
            End If
        End If
    End Sub

End Module
