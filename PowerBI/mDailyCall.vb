﻿Module mDailyCall
    Private sDate As String
    Private sCurDate As String
    Private sStartDate As String
    Private sEndDate As String
    Private lDay As Long

    Public Sub DailyCall()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.DailyCallVolume "
        clR.RunSQL(SQL, sCON)
        sDate = "10/3/2021"
        lDay = DateDiff(DateInterval.Day, CDate(sDate), Today)
        For cnt = 0 To lDay + 1
            sStartDate = DateAdd(DateInterval.Day, cnt, CDate(sDate))
            sEndDate = DateAdd(DateInterval.Day, 1, CDate(sStartDate))
            If CDate(sStartDate) >= Today Then
                GoTo MoveNext
            End If
            InsertRows()
            GetClaimsInbound()
            GetClaimsOutbound()
            GetCSInbound()
            GetDealerInbound()
            GetDealerOutbound()
            GetPaymentsInbound()
            GetPaymentsOutbound()
MoveNext:
        Next
    End Sub

    Private Sub GetPaymentsOutbound()
        Dim clC As New clsDBO
        Dim clR As New clsDBO
        SQL = "select sum(outboundcount) as INC from veritasphone.dbo.GroupDailyCallVolume "
        SQL = SQL + "where callgroup in ('Payments Main')  "
        SQL = SQL + "and calldate >= '" & sStartDate & "' "
        SQL = SQL + "and calldate < '" & sEndDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.DailyCallVolume "
            SQL = SQL + "set calls = " & clC.Fields("inc") & " "
            SQL = SQL + "where statdate = '" & sStartDate & "' "
            SQL = SQL + "and calldept = 'Payments Outbound' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub GetPaymentsInbound()
        Dim clC As New clsDBO
        Dim clR As New clsDBO
        SQL = "select sum(inboundcount) as INC from veritasphone.dbo.GroupDailyCallVolume "
        SQL = SQL + "where callgroup in ('Payments Main')  "
        SQL = SQL + "and calldate >= '" & sStartDate & "' "
        SQL = SQL + "and calldate < '" & sEndDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.DailyCallVolume "
            SQL = SQL + "set calls = " & clC.Fields("inc") & " "
            SQL = SQL + "where statdate = '" & sStartDate & "' "
            SQL = SQL + "and calldept = 'Payments Inbound' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub GetDealerOutbound()
        Dim clC As New clsDBO
        Dim clR As New clsDBO
        SQL = "select sum(outboundcount) as INC from veritasphone.dbo.GroupDailyCallVolume "
        SQL = SQL + "where callgroup in ('Dealer Support Main')  "
        SQL = SQL + "and calldate >= '" & sStartDate & "' "
        SQL = SQL + "and calldate < '" & sEndDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.DailyCallVolume "
            SQL = SQL + "set calls = " & clC.Fields("inc") & " "
            SQL = SQL + "where statdate = '" & sStartDate & "' "
            SQL = SQL + "and calldept = 'Dealer Services Outbound' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub GetDealerInbound()
        Dim clC As New clsDBO
        Dim clR As New clsDBO
        SQL = "select sum(inboundcount) as INC from veritasphone.dbo.GroupDailyCallVolume "
        SQL = SQL + "where callgroup in ('Dealer Support Main')  "
        SQL = SQL + "and calldate >= '" & sStartDate & "' "
        SQL = SQL + "and calldate < '" & sEndDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.DailyCallVolume "
            SQL = SQL + "set calls = " & clC.Fields("inc") & " "
            SQL = SQL + "where statdate = '" & sStartDate & "' "
            SQL = SQL + "and calldept = 'Dealer Services Inbound' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub GetCSInbound()
        Dim clC As New clsDBO
        Dim clR As New clsDBO
        SQL = "select sum(inboundcount) as INC from veritasphone.dbo.GroupDailyCallVolume "
        SQL = SQL + "where callgroup in ('Customer Service Main', 'AN Cust Svc')  "
        SQL = SQL + "and calldate >= '" & sStartDate & "' "
        SQL = SQL + "and calldate < '" & sEndDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.DailyCallVolume "
            SQL = SQL + "set calls = " & clC.Fields("inc") & " "
            SQL = SQL + "where statdate = '" & sStartDate & "' "
            SQL = SQL + "and calldept = 'CS Inbound' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub GetClaimsOutbound()
        Dim clC As New clsDBO
        Dim clR As New clsDBO
        SQL = "select sum(outboundcount) as INC from veritasphone.dbo.GroupDailyCallVolume "
        SQL = SQL + "where callgroup in ('AN Powertrain', 'AN Non-PT', 'Gen Claims')  "
        SQL = SQL + "and calldate >= '" & sStartDate & "' "
        SQL = SQL + "and calldate < '" & sEndDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.DailyCallVolume "
            SQL = SQL + "set calls = " & clC.Fields("inc") & " "
            SQL = SQL + "where statdate = '" & sStartDate & "' "
            SQL = SQL + "and calldept = 'Claims Outbound' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub GetClaimsInbound()
        Dim clC As New clsDBO
        Dim clR As New clsDBO
        SQL = "select sum(inboundcount) as INC from veritasphone.dbo.GroupDailyCallVolume "
        SQL = SQL + "where callgroup in ('AN Powertrain', 'AN Non-PT', 'Gen Claims')  "
        SQL = SQL + "and calldate >= '" & sStartDate & "' "
        SQL = SQL + "and calldate < '" & sEndDate & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "update veritaspowerbi.dbo.DailyCallVolume "
            SQL = SQL + "set calls = " & clC.Fields("inc") & " "
            SQL = SQL + "where statdate = '" & sStartDate & "' "
            SQL = SQL + "and calldept = 'Claims Inbound' "
            clR.RunSQL(SQL, sCON)
        End If
    End Sub

    Private Sub InsertRows()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.DailyCallVolume "
        SQL = SQL + "(calldept, statdate, calls) "
        SQL = SQL + "values ('Claims Inbound', '" & sStartDate & "', 0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.DailyCallVolume "
        SQL = SQL + "(calldept, statdate, calls) "
        SQL = SQL + "values ('CS Inbound', '" & sStartDate & "', 0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.DailyCallVolume "
        SQL = SQL + "(calldept, statdate, calls) "
        SQL = SQL + "values ('Dealer Services Inbound', '" & sStartDate & "', 0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.DailyCallVolume "
        SQL = SQL + "(calldept, statdate, calls) "
        SQL = SQL + "values ('Payments Inbound', '" & sStartDate & "', 0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.DailyCallVolume "
        SQL = SQL + "(calldept, statdate, calls) "
        SQL = SQL + "values ('Claims Outbound', '" & sStartDate & "', 0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.DailyCallVolume "
        SQL = SQL + "(calldept, statdate, calls) "
        SQL = SQL + "values ('CS Outbound', '" & sStartDate & "', 0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.DailyCallVolume "
        SQL = SQL + "(calldept, statdate, calls) "
        SQL = SQL + "values ('Dealer Services Outbound', '" & sStartDate & "', 0)"
        clR.RunSQL(SQL, sCON)
        SQL = "insert into veritaspowerbi.dbo.DailyCallVolume "
        SQL = SQL + "(calldept, statdate, calls) "
        SQL = SQL + "values ('Payments Outbound', '" & sStartDate & "', 0)"
        clR.RunSQL(SQL, sCON)

    End Sub


End Module
