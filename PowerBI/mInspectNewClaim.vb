﻿Module mInspectNewClaim
    Private sStartDate As String
    Private sEndDate As String
    Private lInspect As Long
    Private lNewClaim As Long

    Public Sub InspectNewClaim()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.inspectnewclaim "
        clR.RunSQL(SQL, sCON)
        sStartDate = "1/1/2020"
        Do Until CDate(sStartDate) > Today
            sEndDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
            CalcInspection()
            CalcNewClaim()

            SQL = "insert into veritaspowerbi.dbo.inspectnewclaim "
            SQL = SQL + "(monthdate, inspection, newclaims) "
            SQL = SQL + "values ('" & sStartDate & "', "
            SQL = SQL & lInspect & ", "
            SQL = SQL & lNewClaim & ""
            SQL = SQL + ")"
            clR.RunSQL(SQL, sCON)
            sStartDate = sEndDate
        Loop

        CalcRunRate()
    End Sub

    Private Sub CalcRunRate()
        Dim sStartDate As String
        Dim sEndDate As String
        Dim lMaxDay As String
        Dim lDays As Long
        Dim SQL As String
        Dim clR As New clsDBO
        Dim lTemp As Long
        lDays = Day(Today)
        sStartDate = Month(Today) & "/1/" & Year(Today)
        sEndDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
        lMaxDay = DateDiff(DateInterval.Day, CDate(sStartDate), CDate(sEndDate))
        SQL = "select * from veritaspowerbi.dbo.inspectnewclaim "
        SQL = SQL + "where monthdate = '" & sStartDate & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("inspection").Length > 0 Then
                lTemp = clR.Fields("inspection")
                lTemp = (lTemp / lDays) * lMaxDay
                clR.Fields("inspection") = lTemp
            End If
            If clR.Fields("newclaims").Length > 0 Then
                lTemp = clR.Fields("newclaims")
                lTemp = (lTemp / lDays) * lMaxDay
                clR.Fields("newclaims") = lTemp
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub CalcNewClaim()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from claim "
        SQL = SQL + "where credate >= '" & sStartDate & "' "
        SQL = SQL + "and credate < '" & sEndDate & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lNewClaim = clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcInspection()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from claiminspection "
        SQL = SQL + "where requestdate >= '" & sStartDate & "' "
        SQL = SQL + "and requestdate <= '" & sEndDate & "' "
        SQL = SQL + "and not inspectionid is null "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lInspect = clR.Fields("cnt")
            End If
        End If
    End Sub
End Module
