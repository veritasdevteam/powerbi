﻿Module mSalesVolumeAgent
    Private sStartDate As String
    Private sEndDate As String
    Private sAgent1 As String
    Private sAgent2 As String


    Public Sub SalesVolumeAgent()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.salesvolumeagent "
        clR.RunSQL(SQL, sCON)
        GetTop2Agents()
        sStartDate = Month(Today) & "/1/" & Year(Today)
        sStartDate = DateAdd(DateInterval.Year, -2, CDate(sStartDate))
        Do Until CDate(sStartDate) > Today
            sEndDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
            CalcAN()
            CalcEP()
            CalcAgent1()
            CalcAgent2()
            CalcOther()
            sStartDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
        Loop
        CalcRunRate()
    End Sub

    Private Sub GetTop2Agents()
        Dim lMonth As Long
        Dim clR As New clsDBO
        lMonth = Month(Today)
        lMonth = lMonth - 2
        SQL = "select AgentName, count(*) from VeritasMoxy.dbo.MoxyContract "
        SQL = SQL + "where solddate > '" & lMonth & "/1/" & Year(Today) & "' "
        SQL = SQL + "and AgentName <> 'Auto Nation Agent'
        group by AgentName
        order by count(*) desc "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To 1
                clR.GetRowNo(cnt)
                If cnt = 0 Then
                    sAgent1 = clR.Fields("agentname")
                End If
                If cnt = 1 Then
                    sAgent2 = clR.Fields("agentname")
                End If
            Next
        End If
    End Sub

    Private Sub CalcRunRate()
        Dim sMonth As String
        Dim lDay As Long
        Dim lTotalDays As Long
        Dim clR As New clsDBO
        Dim clM As New clsDBO
        Dim lTemp As Long
        Dim sPrevMonth As String
        sMonth = Month(Today) & "/1/" & Year(Today)
        lDay = Day(Today)
        lDay = lDay - 1
        lTotalDays = DateDiff(DateInterval.Day, CDate(sMonth), DateAdd(DateInterval.Month, 1, CDate(sMonth)))
        sPrevMonth = DateAdd(DateInterval.Month, -1, CDate(sMonth))
        SQL = "select * from veritaspowerbi.dbo.salesvolumeagent "
        SQL = SQL + "where monthdate = '" & sMonth & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from veritaspowerbi.dbo.salesvolumeagent "
                SQL = SQL + "where idx = " & clR.Fields("idx")
                clM.OpenDB(SQL, sCON)
                If clM.RowCount > 0 Then
                    clM.GetRow()
                    lTemp = clM.Fields("cnt")
                    If lTemp > 0 Then
                        lTemp = (lTemp / lDay) * lTotalDays
                    End If
                    clM.Fields("cnt") = lTemp
                    clM.SaveDB()
                End If

            Next
        End If
    End Sub

    Private Sub CalcOther()
        Dim lCnt As Long
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.moxycontract mc "
        SQL = SQL + "inner join Dealer d on d.DealerName = mc.DealerName "
        SQL = SQL + "and not d.Prime <> 0 "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and solddate < '" & sEndDate & "' "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and not mc.AgentName like 'auto nation%' "
        SQL = SQL + "and not mc.agentname = '" & sAgent1 & "' "
        SQL = SQL + "and not mc.agentname = '" & sAgent2 & "' "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
        End If
        SQL = "insert into veritaspowerbi.dbo.salesvolumeagent "
        SQL = SQL + "(monthdate, agent, cnt) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartDate & "', "
        SQL = SQL + "'Other Business',"
        SQL = SQL & lCnt & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcPrime()
        Dim lCnt As Long
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.moxycontract mc "
        SQL = SQL + "inner join Dealer d on d.DealerName = mc.DealerName "
        SQL = SQL + "and d.Prime <> 0 "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and solddate < '" & sEndDate & "' "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and mc.AgentName like 't.a.%' "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
        End If
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.apccontract mc "
        SQL = SQL + "inner join Dealer d on d.DealerName = mc.DealerName "
        SQL = SQL + "and d.Prime <> 0 "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and solddate < '" & sEndDate & "' "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = lCnt + clR.Fields("cnt")
        End If
        SQL = "insert into veritaspowerbi.dbo.salesvolumeagent "
        SQL = SQL + "(monthdate, agent, cnt) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartDate & "', "
        SQL = SQL + "'Prime',"
        SQL = SQL & lCnt & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcTASA()
        Dim lCnt As Long
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.moxycontract mc "
        SQL = SQL + "inner join Dealer d on d.DealerName = mc.DealerName "
        SQL = SQL + "and not d.Prime <> 0 "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and solddate < '" & sEndDate & "' "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and mc.AgentName like 't.a.%' "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
        End If
        SQL = "insert into veritaspowerbi.dbo.salesvolumeagent "
        SQL = SQL + "(monthdate, agent, cnt) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartDate & "', "
        SQL = SQL + "'TASA',"
        SQL = SQL & lCnt & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcEP()
        Dim lCnt As Long
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.epcontract mc "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and solddate < '" & sEndDate & "' "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
        End If
        SQL = "insert into veritaspowerbi.dbo.salesvolumeagent "
        SQL = SQL + "(monthdate, agent, cnt) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartDate & "', "
        SQL = SQL + "'Express Protect',"
        SQL = SQL & lCnt & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcCBerman()
        Dim lCnt As Long
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.moxycontract mc "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and solddate < '" & sEndDate & "' "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and mc.mga like '%c. berman%' "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
        End If
        SQL = "insert into veritaspowerbi.dbo.salesvolumeagent "
        SQL = SQL + "(monthdate, agent, cnt) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartDate & "', "
        SQL = SQL + "'C Berman',"
        SQL = SQL & lCnt & ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcAgent1()
        Dim lCnt As Long
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.moxycontract mc "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and solddate < '" & sEndDate & "' "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and mc.AgentName = '" & sAgent1 & "' "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
        End If
        SQL = "insert into veritaspowerbi.dbo.salesvolumeagent "
        SQL = SQL + "(monthdate, agent, cnt) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartDate & "', "
        SQL = SQL + "'" & sAgent1 & "',"
        SQL = SQL & lCnt & ")"
        clR.RunSQL(SQL, sCON)

    End Sub

    Private Sub CalcAgent2()
        Dim lCnt As Long
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.moxycontract mc "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and solddate < '" & sEndDate & "' "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and mc.AgentName = '" & sAgent2 & "' "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
        End If
        SQL = "insert into veritaspowerbi.dbo.salesvolumeagent "
        SQL = SQL + "(monthdate, agent, cnt) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartDate & "', "
        SQL = SQL + "'" & sAgent2 & "',"
        SQL = SQL & lCnt & ")"
        clR.RunSQL(SQL, sCON)

    End Sub

    Private Sub CalcAN()
        Dim lCnt As Long
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.moxycontract mc "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and solddate < '" & sEndDate & "' "
        SQL = SQL + "and (dealstatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and mc.AgentName like 'auto nation%' "
        SQL = SQL + "and program <> 'productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCnt = clR.Fields("cnt")
        End If
        SQL = "insert into veritaspowerbi.dbo.salesvolumeagent "
        SQL = SQL + "(monthdate, agent, cnt) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartDate & "', "
        SQL = SQL + "'AutoNation',"
        SQL = SQL & lCnt & ")"
        clR.RunSQL(SQL, sCON)

    End Sub

End Module
