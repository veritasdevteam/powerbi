﻿Module mSalesVolume
    Private sStartDate As String
    Private sEndDate As String
    Private lCancelITD As Long
    Private lActual As Long
    Private lSeries6 As Long
    Private lAvgDay As Long
    Private lMonth6Avg As Long
    Private lSales As Long

    Public Sub SalesVolume()
        Dim clR As New clsDBO
        SQL = "truncate table veritaspowerbi.dbo.salesvolume "
        clR.RunSQL(SQL, sCON)
        sStartDate = Month(Today) & "/1/" & Year(Today)
        sStartDate = DateAdd(DateInterval.Year, -2, CDate(sStartDate))
        Do Until CDate(sStartDate) > Today
            sEndDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
            CalcCancelITD()
            CalcActual()
            CalcSales()
            CalcMonth6()
            AddData()
            sStartDate = DateAdd(DateInterval.Month, 1, CDate(sStartDate))
        Loop
        CalcRunRate()

    End Sub

    Private Sub CalcRunRate()
        Dim sMonth As String
        Dim lDay As Long
        Dim lTotalDays As Long
        Dim clR As New clsDBO
        Dim lTemp As Long
        Dim sPrevMonth As String
        Dim lPrevCancelAmt As Long
        sMonth = Month(Today) & "/1/" & Year(Today)
        lDay = Day(Today)
        lDay = lDay - 1
        lTotalDays = DateDiff(DateInterval.Day, CDate(sMonth), DateAdd(DateInterval.Month, 1, CDate(sMonth)))
        sPrevMonth = DateAdd(DateInterval.Month, -1, CDate(sMonth))
        SQL = "select * from veritaspowerbi.dbo.salesvolume "
        SQL = SQL + "where monthdate = '" & sPrevMonth & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lPrevCancelAmt = clR.Fields("cancelitd")
        End If
        SQL = "select * from veritaspowerbi.dbo.salesvolume "
        SQL = SQL + "where monthdate = '" & sMonth & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lTemp = CLng(clR.Fields("cancelitd")) - lPrevCancelAmt
            If lDay > 0 Then
                lTemp = (lTemp / lDay) * lTotalDays
            End If
            lTemp = lTemp + lPrevCancelAmt
            clR.Fields("cancelitd") = lTemp
            lTemp = clR.Fields("actual")
            If lTemp > 0 Then
                lTemp = (lTemp / lDay) * lTotalDays
            End If
            clR.Fields("actual") = lTemp
            lTemp = clR.Fields("series6")
            If lTemp > 0 Then
                lTemp = (lTemp / lDay) * lTotalDays
            End If
            clR.Fields("series6") = lTemp
            clR.SaveDB()
        End If
    End Sub

    Private Sub AddData()
        Dim clR As New clsDBO
        SQL = "insert into veritaspowerbi.dbo.salesvolume "
        SQL = SQL + "(monthdate, cancelitd, actual, sales, series6, salesperday,month6avg) "
        SQL = SQL + "values ('"
        SQL = SQL + sStartDate & "',"
        SQL = SQL & lCancelITD & ","
        SQL = SQL & lActual & ","
        SQL = SQL & lSales & ","
        SQL = SQL & lSeries6 & ","
        SQL = SQL & lAvgDay & ","
        SQL = SQL & lMonth6Avg & ""
        SQL = SQL + ")"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub CalcMonth6()
        Dim lDay As Long
        Dim sStart As String
        sStart = DateAdd(DateInterval.Month, -6, CDate(sEndDate))
        lMonth6Avg = 0
        CalcMoxyContract6()
        CalcEPContract6()
        CalcAPCContract6()
        lDay = DateDiff(DateInterval.Day, CDate(sStart), CDate(sEndDate))
        lMonth6Avg = lMonth6Avg / lDay

    End Sub

    Private Sub CalcActual()
        Dim lDay As Long
        lActual = 0
        lSeries6 = 0
        CalcMoxyContract()
        CalcProduct()
        lDay = DateDiff(DateInterval.Day, CDate(sStartDate), CDate(sEndDate))
        lDay = lDay
        lAvgDay = lActual / lDay

    End Sub

    Private Sub CalcSales()
        lSales = 0
        CalcMoxySales()
    End Sub

    Private Sub CalcProduct()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.moxyproduct mp "
        SQL = SQL + "inner join VeritasMoxy.dbo.MoxyContract mc on mp.DealID = mc.DealID "
        SQL = SQL + "where mp.solddate >= '" & sStartDate & "' "
        SQL = SQL + "and mp.SoldDate < '" & sEndDate & "' "
        SQL = SQL + "and (DealStatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt") Then
                lSeries6 = lSeries6 + clR.Fields("cnt")
            End If
        End If
    End Sub
    Private Sub CalcAPCSales()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.apccontract "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and SoldDate < '" & sEndDate & "' "
        SQL = SQL + "and (DealStatus = 'Sold') "
        SQL = SQL + "and Program <> 'Productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lSales = lSales + clR.Fields("cnt")
            End If
        End If
    End Sub


    Private Sub CalcEPSales()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.epcontract "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and SoldDate < '" & sEndDate & "' "
        SQL = SQL + "and (DealStatus = 'Sold') "
        SQL = SQL + "and Program <> 'Productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lSales = lSales + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcMoxySales()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where saledate >= '" & sStartDate & "' "
        SQL = SQL + "and saleDate < '" & sEndDate & "' "
        SQL = SQL + "and status in ('Pending', 'Paid', 'Expired') "
        SQL = SQL + "and not programid in (47,48, 51,54,55,56,57,58,59,60,103,105,67) "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lSales = lSales + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcAPCContract6()
        Dim clR As New clsDBO
        Dim sStart As String
        sStart = DateAdd(DateInterval.Month, -6, CDate(sEndDate))
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.apccontract "
        SQL = SQL + "where solddate >= '" & sStart & "' "
        SQL = SQL + "and SoldDate < '" & sEndDate & "' "
        SQL = SQL + "and (DealStatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and Program <> 'Productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lMonth6Avg = lMonth6Avg + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcAPCContract()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.apccontract "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and SoldDate < '" & sEndDate & "' "
        SQL = SQL + "and (DealStatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and Program <> 'Productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("cnt").Length > 0 Then
                lActual = lActual + clR.Fields("cnt")
                lSeries6 = lSeries6 + clR.Fields("cnt")
            End If
        End If
    End Sub

    Private Sub CalcEPContract6()
        Dim clR As New clsDBO
        Dim sStart As String
        sStart = DateAdd(DateInterval.Month, -6, CDate(sEndDate))
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.epcontract "
        SQL = SQL + "where solddate >= '" & sStart & "' "
        SQL = SQL + "and SoldDate < '" & sEndDate & "' "
        SQL = SQL + "and (DealStatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and Program <> 'Productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lMonth6Avg = lMonth6Avg + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcEPContract()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.epcontract "
        SQL = SQL + "where solddate >= '" & sStartDate & "' "
        SQL = SQL + "and SoldDate < '" & sEndDate & "' "
        SQL = SQL + "and (DealStatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and Program <> 'Productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lActual = lActual + clR.Fields("cnt")
            lSeries6 = lSeries6 + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcMoxyContract6()
        Dim clR As New clsDBO
        Dim sStart As String
        sStart = DateAdd(DateInterval.Month, -6, CDate(sEndDate))
        SQL = "select count(*) as cnt from VeritasMoxy.dbo.moxycontract "
        SQL = SQL + "where solddate >= '" & sStart & "' "
        SQL = SQL + "and SoldDate < '" & sEndDate & "' "
        SQL = SQL + "and (DealStatus = 'Sold' "
        SQL = SQL + "or dealstatus = 'Cancelled') "
        SQL = SQL + "and Program <> 'Productonly' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lMonth6Avg = lMonth6Avg + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcMoxyContract()
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from contract "
        SQL = SQL + "where saledate >= '" & sStartDate & "' "
        SQL = SQL + "and saleDate < '" & sEndDate & "' "
        SQL = SQL + "and status in ('Pending', 'Paid', 'Expired', 'Cancelled') "
        SQL = SQL + "and not programid in (47,48,51,54,55,56,57,58,59,60,103,105,67)"
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lActual = lActual + clR.Fields("cnt")
            lSeries6 = lSeries6 + clR.Fields("cnt")
        End If
    End Sub

    Private Sub CalcCancelITD()
        Dim clR As New clsDBO
        SQL = "select count(distinct c.contractid) as cnt from contract c "
        SQL = SQL + "inner join contractcancel cc on cc.contractid = c.contractid "
        SQL = SQL + "where cc.canceldate < '" & sEndDate & "' "
        SQL = SQL + "and not ProgramID in (47,48,51,54,55,56,57,58,59,60,101,103,105,67) "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            lCancelITD = clR.Fields("cnt")
        End If
    End Sub
End Module
